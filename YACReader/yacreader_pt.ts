<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>BookmarksDialog</name>
    <message>
        <location filename="bookmarks_dialog.cpp" line="25"/>
        <source>Lastest Page</source>
        <translation>Última Página</translation>
    </message>
    <message>
        <location filename="bookmarks_dialog.cpp" line="73"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="bookmarks_dialog.cpp" line="83"/>
        <source>Click on any image to go to the bookmark</source>
        <translation>Clique em qualquer imagem para ir para o marcador</translation>
    </message>
    <message>
        <location filename="bookmarks_dialog.cpp" line="107"/>
        <location filename="bookmarks_dialog.cpp" line="131"/>
        <source>Loading...</source>
        <translation>Carregando...</translation>
    </message>
</context>
<context>
    <name>FileComic</name>
    <message>
        <location filename="../common/comic.cpp" line="309"/>
        <source>Unknown error opening the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="418"/>
        <source>7z not found</source>
        <translation type="unfinished">7z não encontrado</translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="424"/>
        <source>Format not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="302"/>
        <source>CRC error on page (%1): some of the pages will not be displayed correctly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <location filename="goto_dialog.cpp" line="17"/>
        <source>Page : </source>
        <translation>Página : </translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="25"/>
        <source>Go To</source>
        <translation>Ir Para</translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="27"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="41"/>
        <location filename="goto_dialog.cpp" line="76"/>
        <source>Total pages : </source>
        <translation>Total de páginas : </translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="55"/>
        <source>Go to...</source>
        <translation>Ir para...</translation>
    </message>
</context>
<context>
    <name>GoToFlowToolBar</name>
    <message>
        <location filename="goto_flow_toolbar.cpp" line="44"/>
        <source>Page : </source>
        <translation>Página : </translation>
    </message>
</context>
<context>
    <name>HelpAboutDialog</name>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="21"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="24"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
</context>
<context>
    <name>MainWindowViewer</name>
    <message>
        <location filename="main_window_viewer.cpp" line="243"/>
        <source>&amp;Open</source>
        <translation>&amp;Abrir</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="244"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="246"/>
        <source>Open a comic</source>
        <translation>Abrir um quadrinho</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="249"/>
        <source>Open Folder</source>
        <translation>Abrir Pasta</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="250"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="252"/>
        <source>Open image folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="255"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="257"/>
        <location filename="main_window_viewer.cpp" line="727"/>
        <source>Save current page</source>
        <translation>Salvar página atual</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="261"/>
        <source>Previous Comic</source>
        <translation>Quadrinho Anterior</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="264"/>
        <source>Open previous comic</source>
        <translation>Abrir quadrinho anterior</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="268"/>
        <source>Next Comic</source>
        <translation>Próximo Quadrinho</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="271"/>
        <source>Open next comic</source>
        <translation>Abrir próximo quadrinho</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="275"/>
        <source>&amp;Previous</source>
        <translation>A&amp;nterior</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="279"/>
        <source>Go to previous page</source>
        <translation>Ir para a página anterior</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="283"/>
        <source>&amp;Next</source>
        <translation>&amp;Próxima</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="287"/>
        <source>Go to next page</source>
        <translation>Ir para a próxima página</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="300"/>
        <source>Fit Width</source>
        <translation>Ajustar à Largura</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="296"/>
        <source>Fit image to height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="291"/>
        <source>Fit Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="305"/>
        <source>Fit image to width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="309"/>
        <source>Rotate image to the left</source>
        <translation>Girar imagem à esquerda</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="310"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="315"/>
        <source>Rotate image to the right</source>
        <translation>Girar imagem à direita</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="316"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="321"/>
        <source>Double page mode</source>
        <translation>Modo dupla página</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="322"/>
        <source>Switch to double page mode</source>
        <translation>Alternar para o modo dupla página</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="323"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="330"/>
        <source>Go To</source>
        <translation>Ir Para</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="331"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="334"/>
        <source>Go to page ...</source>
        <translation>Ir para a página...</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="337"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="338"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="339"/>
        <source>YACReader options</source>
        <translation>Opções do YACReader</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="344"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="345"/>
        <source>Help, About YACReader</source>
        <translation>Ajuda, Sobre o YACReader</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="350"/>
        <source>Magnifying glass</source>
        <translation>Lupa</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="351"/>
        <source>Switch Magnifying glass</source>
        <translation>Alternar Lupa</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="352"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="358"/>
        <source>Set bookmark</source>
        <translation>Definir marcador</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="359"/>
        <source>Set a bookmark on the current page</source>
        <translation>Definir um marcador na página atual</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="368"/>
        <source>Show bookmarks</source>
        <translation>Mostrar marcadores</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="369"/>
        <source>Show the bookmarks of the current comic</source>
        <translation>Mostrar os marcadores do quadrinho atual</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="370"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="375"/>
        <source>Show keyboard shortcuts</source>
        <translation>Mostrar teclas de atalhos</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="379"/>
        <source>Show Info</source>
        <translation>Mostrar Informações</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="380"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="385"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="390"/>
        <source>Show Dictionary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="397"/>
        <source>Always on top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="405"/>
        <source>Show full size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="413"/>
        <source>Show go to flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="422"/>
        <source>&amp;File</source>
        <translation>&amp;Arquivo</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="590"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="616"/>
        <source>Open Comic</source>
        <translation>Abrir Quadrinho</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="616"/>
        <source>Comic files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="943"/>
        <source>Remind me in 14 days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="944"/>
        <source>Not now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="671"/>
        <source>Open folder</source>
        <translation>Abrir pasta</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="727"/>
        <source>Image files (*.jpg)</source>
        <translation>Arquivos de imagem (*.jpg)</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="727"/>
        <source>page_%1.jpg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="939"/>
        <source>There is a new version available</source>
        <translation>Há uma nova versão disponível</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="940"/>
        <source>Do you want to download the new version?</source>
        <translation>Você deseja baixar a nova versão?</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="options_dialog.cpp" line="35"/>
        <source>&quot;Go to flow&quot; size</source>
        <translation>Tamanho do &quot;Ir para cheia&quot;</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="46"/>
        <source>My comics path</source>
        <translation>Meu caminho de quadrinhos</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="56"/>
        <source>Page width stretch</source>
        <translation>Trecho da largura da página</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="77"/>
        <source>Background color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="80"/>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="108"/>
        <source>Restart is needed</source>
        <translation>Reiniciar é necessário</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="121"/>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="122"/>
        <source>Contrast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="123"/>
        <source>Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="127"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="133"/>
        <source>Image options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="143"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="144"/>
        <source>Page Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="145"/>
        <source>Image adjustment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="158"/>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="165"/>
        <source>Comics directory</source>
        <translation>Diretório de quadrinhos</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>7z lib not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>unable to load 7z lib from ./utils</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutsDialog</name>
    <message>
        <location filename="shortcuts_dialog.cpp" line="16"/>
        <source>YACReader keyboard shortcuts</source>
        <translation>Teclas de atalhos do YACReader</translation>
    </message>
    <message>
        <location filename="shortcuts_dialog.cpp" line="20"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="shortcuts_dialog.cpp" line="66"/>
        <source>Keyboard Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Viewer</name>
    <message>
        <location filename="viewer.cpp" line="42"/>
        <location filename="viewer.cpp" line="693"/>
        <source>Press &apos;O&apos; to open comic.</source>
        <translation>Pressione &apos;O&apos; para abrir um quadrinho.</translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="217"/>
        <source>Not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="217"/>
        <source>Comic not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="223"/>
        <source>Error opening comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="229"/>
        <source>CRC Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="706"/>
        <source>Loading...please wait!</source>
        <translation>Carregando... por favor, aguarde!</translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="717"/>
        <source>Page not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="853"/>
        <source>Cover!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="870"/>
        <source>Last page!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderFieldEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="29"/>
        <source>Click to overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="11"/>
        <source>Restore to default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderFieldPlainTextEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="20"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="45"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="51"/>
        <source>Click to overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="10"/>
        <source>Restore to default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="13"/>
        <source>How to show covers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="15"/>
        <source>CoverFlow look</source>
        <translation>Olhar capa cheia</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="16"/>
        <source>Stripe look</source>
        <translation>Olhar lista</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="17"/>
        <source>Overlapped Stripe look</source>
        <translation>Olhar lista sobreposta</translation>
    </message>
</context>
<context>
    <name>YACReaderGLFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="19"/>
        <source>Presets:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="21"/>
        <source>Classic look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="24"/>
        <source>Stripe look</source>
        <translation>Olhar lista</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="27"/>
        <source>Overlapped Stripe look</source>
        <translation>Olhar lista sobreposta</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="30"/>
        <source>Modern look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="33"/>
        <source>Roulette look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="77"/>
        <source>Show advanced settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="86"/>
        <source>Custom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="89"/>
        <source>View angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="95"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="101"/>
        <source>Cover gap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="107"/>
        <source>Central gap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="113"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="119"/>
        <source>Y offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="125"/>
        <source>Z offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="131"/>
        <source>Cover Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="137"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="143"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="149"/>
        <source>Max angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="181"/>
        <source>Low Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="183"/>
        <source>High Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="194"/>
        <source>Use VSync (improve the image quality in fullscreen mode, worse performance)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="202"/>
        <source>Performance:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderOptionsDialog</name>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="21"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="30"/>
        <source>Use hardware acceleration (restart needed)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderTranslator</name>
    <message>
        <location filename="translator.cpp" line="62"/>
        <source>YACReader translator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translator.cpp" line="117"/>
        <location filename="translator.cpp" line="210"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translator.cpp" line="140"/>
        <source>clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translator.cpp" line="219"/>
        <source>Service not available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
