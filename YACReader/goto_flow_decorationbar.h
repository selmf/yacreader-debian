#ifndef GOTO_FLOW_DECORATIONBAR_H
#define GOTO_FLOW_DECORATIONBAR_H

#include <QWidget>

class GoToFlowDecorationBar : public QWidget
{
	Q_OBJECT
	public:
		GoToFlowDecorationBar(QWidget * parent = 0);
};

#endif