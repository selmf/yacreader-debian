<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_BE" sourcelanguage="en">
<context>
    <name>BookmarksDialog</name>
    <message>
        <location filename="bookmarks_dialog.cpp" line="25"/>
        <source>Lastest Page</source>
        <translation type="unfinished">Laatste Pagina</translation>
    </message>
    <message>
        <location filename="bookmarks_dialog.cpp" line="73"/>
        <source>Close</source>
        <translation type="unfinished">Sluiten</translation>
    </message>
    <message>
        <location filename="bookmarks_dialog.cpp" line="83"/>
        <source>Click on any image to go to the bookmark</source>
        <translation type="unfinished">Klik op een afbeelding om naar de bladwijzer te gaan </translation>
    </message>
    <message>
        <location filename="bookmarks_dialog.cpp" line="107"/>
        <location filename="bookmarks_dialog.cpp" line="131"/>
        <source>Loading...</source>
        <translation type="unfinished">Inladen...</translation>
    </message>
</context>
<context>
    <name>FileComic</name>
    <message>
        <location filename="../common/comic.cpp" line="309"/>
        <source>Unknown error opening the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="418"/>
        <source>7z not found</source>
        <translation type="unfinished">7Z Archiefbestand niet gevonden</translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="424"/>
        <source>Format not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="302"/>
        <source>CRC error on page (%1): some of the pages will not be displayed correctly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <location filename="goto_dialog.cpp" line="17"/>
        <source>Page : </source>
        <translation type="unfinished">Pagina : </translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="25"/>
        <source>Go To</source>
        <translation type="unfinished">Ga Naar</translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="27"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="41"/>
        <location filename="goto_dialog.cpp" line="76"/>
        <source>Total pages : </source>
        <translation type="unfinished">Totaal aantal pagina&apos;s : </translation>
    </message>
    <message>
        <location filename="goto_dialog.cpp" line="55"/>
        <source>Go to...</source>
        <translation type="unfinished">Ga naar...</translation>
    </message>
</context>
<context>
    <name>GoToFlowToolBar</name>
    <message>
        <location filename="goto_flow_toolbar.cpp" line="44"/>
        <source>Page : </source>
        <translation type="unfinished">Pagina : </translation>
    </message>
</context>
<context>
    <name>HelpAboutDialog</name>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="21"/>
        <source>About</source>
        <translation type="unfinished">Over</translation>
    </message>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="24"/>
        <source>Help</source>
        <translation type="unfinished">Help</translation>
    </message>
</context>
<context>
    <name>MainWindowViewer</name>
    <message>
        <location filename="main_window_viewer.cpp" line="243"/>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Open</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="244"/>
        <source>O</source>
        <translation type="unfinished">O</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="246"/>
        <source>Open a comic</source>
        <translation type="unfinished">Open een strip</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="249"/>
        <source>Open Folder</source>
        <translation type="unfinished">Map Openen</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="250"/>
        <source>Ctrl+O</source>
        <translation type="unfinished">Ctrl+O</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="252"/>
        <source>Open image folder</source>
        <translation type="unfinished">Open afbeeldings map</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="255"/>
        <source>Save</source>
        <translation type="unfinished">Bewaar</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="257"/>
        <location filename="main_window_viewer.cpp" line="727"/>
        <source>Save current page</source>
        <translation type="unfinished">Bewaren huidige pagina</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="261"/>
        <source>Previous Comic</source>
        <translation type="unfinished">Vorige Strip</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="264"/>
        <source>Open previous comic</source>
        <translation type="unfinished">Open de vorige strip</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="268"/>
        <source>Next Comic</source>
        <translation type="unfinished">Volgende Strip</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="271"/>
        <source>Open next comic</source>
        <translation type="unfinished">Open volgende strip</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="275"/>
        <source>&amp;Previous</source>
        <translation type="unfinished">&amp;Vorige</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="279"/>
        <source>Go to previous page</source>
        <translation type="unfinished">Ga naar de vorige pagina</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="283"/>
        <source>&amp;Next</source>
        <translation type="unfinished">&amp;Volgende</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="287"/>
        <source>Go to next page</source>
        <translation type="unfinished">Ga naar de volgende pagina</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="300"/>
        <source>Fit Width</source>
        <translation type="unfinished">Vensterbreedte aanpassen</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="296"/>
        <source>Fit image to height</source>
        <translation type="unfinished">Afbeelding aanpassen aan hoogte</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="291"/>
        <source>Fit Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="305"/>
        <source>Fit image to width</source>
        <translation type="unfinished">Afbeelding aanpassen aan breedte</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="309"/>
        <source>Rotate image to the left</source>
        <translation type="unfinished">Links omdraaien</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="310"/>
        <source>L</source>
        <translation type="unfinished">L</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="315"/>
        <source>Rotate image to the right</source>
        <translation type="unfinished">Rechts omdraaien</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="316"/>
        <source>R</source>
        <translation type="unfinished">R</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="321"/>
        <source>Double page mode</source>
        <translation type="unfinished">Dubbele bladzijde modus</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="322"/>
        <source>Switch to double page mode</source>
        <translation type="unfinished">Naar dubbele bladzijde modus</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="323"/>
        <source>D</source>
        <translation type="unfinished">D</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="330"/>
        <source>Go To</source>
        <translation type="unfinished">Ga Naar</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="331"/>
        <source>G</source>
        <translation type="unfinished">G</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="334"/>
        <source>Go to page ...</source>
        <translation type="unfinished">Ga naar bladzijde ...</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="337"/>
        <source>Options</source>
        <translation type="unfinished">Opties</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="338"/>
        <source>C</source>
        <translation type="unfinished">C</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="339"/>
        <source>YACReader options</source>
        <translation type="unfinished">YACReader opties</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="344"/>
        <source>Help</source>
        <translation type="unfinished">Help</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="345"/>
        <source>Help, About YACReader</source>
        <translation type="unfinished">Help, Over YACReader</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="350"/>
        <source>Magnifying glass</source>
        <translation type="unfinished">Vergrootglas</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="351"/>
        <source>Switch Magnifying glass</source>
        <translation type="unfinished">Overschakelen naar Vergrootglas</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="352"/>
        <source>Z</source>
        <translation type="unfinished">Z</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="358"/>
        <source>Set bookmark</source>
        <translation type="unfinished">Bladwijzer instellen</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="359"/>
        <source>Set a bookmark on the current page</source>
        <translation type="unfinished">Een bladwijzer toevoegen aan de huidige pagina</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="368"/>
        <source>Show bookmarks</source>
        <translation type="unfinished">Bladwijzers weergeven</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="369"/>
        <source>Show the bookmarks of the current comic</source>
        <translation type="unfinished">Toon de bladwijzers van de huidige strip</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="370"/>
        <source>M</source>
        <translation type="unfinished">M</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="375"/>
        <source>Show keyboard shortcuts</source>
        <translation type="unfinished">Toon de sneltoetsen</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="379"/>
        <source>Show Info</source>
        <translation type="unfinished">Info tonen</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="380"/>
        <source>I</source>
        <translation type="unfinished">I</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="385"/>
        <source>Close</source>
        <translation type="unfinished">Sluiten</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="390"/>
        <source>Show Dictionary</source>
        <translation type="unfinished">Woordenlijst weergeven</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="397"/>
        <source>Always on top</source>
        <translation type="unfinished">Altijd op voorgrond</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="405"/>
        <source>Show full size</source>
        <translation type="unfinished">Volledig Scherm </translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="413"/>
        <source>Show go to flow</source>
        <translation type="unfinished">Toon ga naar de Omslagbrowser</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="422"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Bestand</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="590"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="616"/>
        <source>Open Comic</source>
        <translation type="unfinished">Open een Strip</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="616"/>
        <source>Comic files</source>
        <translation type="unfinished">Strip bestanden</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="671"/>
        <source>Open folder</source>
        <translation type="unfinished">Open een Map</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="727"/>
        <source>Image files (*.jpg)</source>
        <translation type="unfinished">Afbeelding bestanden (*.jpg)</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="727"/>
        <source>page_%1.jpg</source>
        <translation type="unfinished">pagina_%1.jpg</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="939"/>
        <source>There is a new version available</source>
        <translation type="unfinished">Er is een nieuwe versie beschikbaar</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="940"/>
        <source>Do you want to download the new version?</source>
        <translation type="unfinished">Wilt u de nieuwe versie downloaden?</translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="943"/>
        <source>Remind me in 14 days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main_window_viewer.cpp" line="944"/>
        <source>Not now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="options_dialog.cpp" line="35"/>
        <source>&quot;Go to flow&quot; size</source>
        <translation type="unfinished">&quot;Naar Omslagbrowser&quot; afmetingen</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="46"/>
        <source>My comics path</source>
        <translation type="unfinished">Pad naar mijn strips</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="56"/>
        <source>Page width stretch</source>
        <translation type="unfinished">Pagina breedte</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="77"/>
        <source>Background color</source>
        <translation type="unfinished">Achtergrondkleur</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="80"/>
        <source>Choose</source>
        <translation type="unfinished">Kies</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="108"/>
        <source>Restart is needed</source>
        <translation type="unfinished">Herstart is nodig</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="121"/>
        <source>Brightness</source>
        <translation type="unfinished">Helderheid</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="122"/>
        <source>Contrast</source>
        <translation type="unfinished">Contrast</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="123"/>
        <source>Gamma</source>
        <translation type="unfinished">Gamma</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="127"/>
        <source>Reset</source>
        <translation type="unfinished">Standaardwaarden terugzetten</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="133"/>
        <source>Image options</source>
        <translation type="unfinished">Afbeelding opties</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="143"/>
        <source>General</source>
        <translation type="unfinished">Algemeen</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="144"/>
        <source>Page Flow</source>
        <translation type="unfinished">Omslagbrowser</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="145"/>
        <source>Image adjustment</source>
        <translation type="unfinished">Beeldaanpassing</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="158"/>
        <source>Options</source>
        <translation type="unfinished">Opties</translation>
    </message>
    <message>
        <location filename="options_dialog.cpp" line="165"/>
        <source>Comics directory</source>
        <translation type="unfinished">Strips map</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>7z lib not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>unable to load 7z lib from ./utils</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutsDialog</name>
    <message>
        <location filename="shortcuts_dialog.cpp" line="16"/>
        <source>YACReader keyboard shortcuts</source>
        <translation type="unfinished">YACReader sneltoetsen</translation>
    </message>
    <message>
        <location filename="shortcuts_dialog.cpp" line="20"/>
        <source>Close</source>
        <translation type="unfinished">Sluiten</translation>
    </message>
    <message>
        <location filename="shortcuts_dialog.cpp" line="66"/>
        <source>Keyboard Shortcuts</source>
        <translation type="unfinished">Sneltoetsen</translation>
    </message>
</context>
<context>
    <name>Viewer</name>
    <message>
        <location filename="viewer.cpp" line="42"/>
        <location filename="viewer.cpp" line="693"/>
        <source>Press &apos;O&apos; to open comic.</source>
        <translation type="unfinished">Druk &apos;O&apos; om een strip te openen.</translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="217"/>
        <source>Not found</source>
        <translation type="unfinished">Niet gevonden</translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="217"/>
        <source>Comic not found</source>
        <translation type="unfinished">Strip niet gevonden</translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="223"/>
        <source>Error opening comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="229"/>
        <source>CRC Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="706"/>
        <source>Loading...please wait!</source>
        <translation type="unfinished">Inladen...even wachten!</translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="717"/>
        <source>Page not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="853"/>
        <source>Cover!</source>
        <translation type="unfinished">Omslag!</translation>
    </message>
    <message>
        <location filename="viewer.cpp" line="870"/>
        <source>Last page!</source>
        <translation type="unfinished">Laatste pagina!</translation>
    </message>
</context>
<context>
    <name>YACReaderFieldEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="29"/>
        <source>Click to overwrite</source>
        <translation type="unfinished">Klik hier om te overschrijven</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="11"/>
        <source>Restore to default</source>
        <translation type="unfinished">Standaardwaarden herstellen</translation>
    </message>
</context>
<context>
    <name>YACReaderFieldPlainTextEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="20"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="45"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="51"/>
        <source>Click to overwrite</source>
        <translation type="unfinished">Klik hier om te overschrijven</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="10"/>
        <source>Restore to default</source>
        <translation type="unfinished">Standaardwaarden herstellen</translation>
    </message>
</context>
<context>
    <name>YACReaderFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="13"/>
        <source>How to show covers:</source>
        <translation type="unfinished">Omslagbladen bekijken:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="15"/>
        <source>CoverFlow look</source>
        <translation type="unfinished">Omslagbrowser uiterlijk </translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="16"/>
        <source>Stripe look</source>
        <translation type="unfinished">Brede band </translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="17"/>
        <source>Overlapped Stripe look</source>
        <translation type="unfinished">Overlappende band</translation>
    </message>
</context>
<context>
    <name>YACReaderGLFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="19"/>
        <source>Presets:</source>
        <translation type="unfinished">Voorinstellingen:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="21"/>
        <source>Classic look</source>
        <translation type="unfinished">Klassiek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="24"/>
        <source>Stripe look</source>
        <translation type="unfinished">Brede band</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="27"/>
        <source>Overlapped Stripe look</source>
        <translation type="unfinished">Overlappende band</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="30"/>
        <source>Modern look</source>
        <translation type="unfinished">Modern</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="33"/>
        <source>Roulette look</source>
        <translation type="unfinished">Roulette</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="77"/>
        <source>Show advanced settings</source>
        <translation type="unfinished">Toon geavanceerde instellingen</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="86"/>
        <source>Custom:</source>
        <translation type="unfinished">Aangepast:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="89"/>
        <source>View angle</source>
        <translation type="unfinished">Kijkhoek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="95"/>
        <source>Position</source>
        <translation type="unfinished">Positie</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="101"/>
        <source>Cover gap</source>
        <translation type="unfinished">Ruimte tss Omslag</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="107"/>
        <source>Central gap</source>
        <translation type="unfinished">Centrale ruimte</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="113"/>
        <source>Zoom</source>
        <translation type="unfinished">Zoom</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="119"/>
        <source>Y offset</source>
        <translation type="unfinished">Y-positie</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="125"/>
        <source>Z offset</source>
        <translation type="unfinished">Z- positie</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="131"/>
        <source>Cover Angle</source>
        <translation type="unfinished">Omslag hoek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="137"/>
        <source>Visibility</source>
        <translation type="unfinished">Zichtbaarheid</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="143"/>
        <source>Light</source>
        <translation type="unfinished">Licht</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="149"/>
        <source>Max angle</source>
        <translation type="unfinished">Maximale hoek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="181"/>
        <source>Low Performance</source>
        <translation type="unfinished">Lage Prestaties</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="183"/>
        <source>High Performance</source>
        <translation type="unfinished">Hoge Prestaties</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="194"/>
        <source>Use VSync (improve the image quality in fullscreen mode, worse performance)</source>
        <translation type="unfinished">Gebruik VSync (verbetering van de beeldkwaliteit in de modus volledig scherm, slechtere prestatie)</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="202"/>
        <source>Performance:</source>
        <translation type="unfinished">Prestatie:</translation>
    </message>
</context>
<context>
    <name>YACReaderOptionsDialog</name>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="21"/>
        <source>Save</source>
        <translation type="unfinished">Bewaar</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="30"/>
        <source>Use hardware acceleration (restart needed)</source>
        <translation type="unfinished">Gebruik hardware versnelling (opnieuw opstarten vereist)</translation>
    </message>
</context>
<context>
    <name>YACReaderTranslator</name>
    <message>
        <location filename="translator.cpp" line="62"/>
        <source>YACReader translator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translator.cpp" line="117"/>
        <location filename="translator.cpp" line="210"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translator.cpp" line="140"/>
        <source>clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="translator.cpp" line="219"/>
        <source>Service not available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
