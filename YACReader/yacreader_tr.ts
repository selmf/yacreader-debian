<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>BookmarksDialog</name>
    <message>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation>Yükleniyor...</translation>
    </message>
    <message>
        <source>Click on any image to go to the bookmark</source>
        <translation>Yer imine git</translation>
    </message>
    <message>
        <source>Lastest Page</source>
        <translation>Son Sayfa</translation>
    </message>
</context>
<context>
    <name>Configuration</name>
    <message>
        <source>There was a problem saving YACReader configuration. Please, check if you have enough permissions in the YACReader root folder.</source>
        <translation type="vanished">Yeni ayarlar kaydedilirken bir problem çıktı. Lütfen YACReader dosyasını açın.</translation>
    </message>
    <message>
        <source>Saving config file....</source>
        <translation type="vanished">Config dosyası kaydediliyor...</translation>
    </message>
</context>
<context>
    <name>FileComic</name>
    <message>
        <source>File not found or not images in file</source>
        <translation type="vanished">Dosya bulunamadı yada dosyada resim yok</translation>
    </message>
    <message>
        <source>7z not found</source>
        <translation>7z bulunamadı</translation>
    </message>
    <message>
        <source>Comic not found</source>
        <translation type="vanished">Çizgi roman bulunamadı</translation>
    </message>
    <message>
        <source>Not found</source>
        <translation type="vanished">Bulunamadı</translation>
    </message>
    <message>
        <source>File error</source>
        <translation type="vanished">Dosya hatası</translation>
    </message>
    <message>
        <source>7z problem</source>
        <translation type="vanished">7z Problemli</translation>
    </message>
    <message>
        <source>7z reading</source>
        <translation type="vanished">7z Okunuyor</translation>
    </message>
    <message>
        <source>7z crashed.</source>
        <translation type="vanished">7z Bozulmuş.</translation>
    </message>
    <message>
        <source>problem reading from 7z</source>
        <translation type="vanished">7z Okunurken Problem Oluştu</translation>
    </message>
    <message>
        <source>7z crashed</source>
        <translation type="vanished">7z Bozulması</translation>
    </message>
    <message>
        <source>Unknown error 7z</source>
        <translation type="vanished">Bilinmeyen 7z hatası</translation>
    </message>
    <message>
        <source>7z wasn&apos;t found in your PATH.</source>
        <translation type="vanished">7z Yolu Bulunamadı.</translation>
    </message>
    <message>
        <source>CRC error on page (%1): some of the pages will not be displayed correctly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown error opening the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format not supported</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <source>Go To</source>
        <translation>Git</translation>
    </message>
    <message>
        <source>Go to...</source>
        <translation>Git...</translation>
    </message>
    <message>
        <source>Total pages : </source>
        <translation>Toplam sayfa:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Vazgeç</translation>
    </message>
    <message>
        <source>Page : </source>
        <translation>Sayfa :</translation>
    </message>
</context>
<context>
    <name>GoToFlowToolBar</name>
    <message>
        <source>Page : </source>
        <translation>Sayfa : </translation>
    </message>
</context>
<context>
    <name>HelpAboutDialog</name>
    <message>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
</context>
<context>
    <name>MainWindowViewer</name>
    <message>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Dosya</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>&amp;İleri</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Aç</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <source>Open Comic</source>
        <translation>Çizgi Romanı Aç</translation>
    </message>
    <message>
        <source>Go To</source>
        <translation>Git</translation>
    </message>
    <message>
        <source>Open image folder</source>
        <translation>Resim dosyasınıaç</translation>
    </message>
    <message>
        <source>Set bookmark</source>
        <translation>Yer imi yap</translation>
    </message>
    <message>
        <source>page_%1.jpg</source>
        <translation>sayfa_%1.jpg</translation>
    </message>
    <message>
        <source>Switch to double page mode</source>
        <translation>Çift sayfa moduna geç</translation>
    </message>
    <message>
        <source>Save current page</source>
        <translation>Geçerli sayfayı kaydet</translation>
    </message>
    <message>
        <source>Double page mode</source>
        <translation>Çift sayfa modu</translation>
    </message>
    <message>
        <source>Switch Magnifying glass</source>
        <translation>Büyüteç</translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation>Dosyayı Aç</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <source>Comic files</source>
        <translation>Çizgi Roman Dosyaları</translation>
    </message>
    <message>
        <source>Go to previous page</source>
        <translation>Önceki sayfaya dön</translation>
    </message>
    <message>
        <source>Open a comic</source>
        <translation>Çizgi romanı aç</translation>
    </message>
    <message>
        <source>Image files (*.jpg)</source>
        <translation>Resim dosyaları (*.jpg)</translation>
    </message>
    <message>
        <source>Next Comic</source>
        <translation>Sırada ki çizgi roman</translation>
    </message>
    <message>
        <source>Saving error log file....</source>
        <translation type="vanished">Hata dosyasını kaydet...</translation>
    </message>
    <message>
        <source>Fit Width</source>
        <translation>Uygun Genişlik</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <source>Show Info</source>
        <translation>Bilgiyi göster</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Dosyayı aç</translation>
    </message>
    <message>
        <source>Go to page ...</source>
        <translation>Sayfata git...</translation>
    </message>
    <message>
        <source>Fit image to width</source>
        <translation>Görüntüyü sığdır</translation>
    </message>
    <message>
        <source>&amp;Previous</source>
        <translation>&amp;Geri</translation>
    </message>
    <message>
        <source>Go to next page</source>
        <translation>Sonra ki sayfaya geç</translation>
    </message>
    <message>
        <source>Show keyboard shortcuts</source>
        <translation>Kılavye kısayollarını göster</translation>
    </message>
    <message>
        <source>Open next comic</source>
        <translation>Sıradaki çizgi romanı aç</translation>
    </message>
    <message>
        <source>There is a new version available</source>
        <translation>Yeni versiyon mevcut</translation>
    </message>
    <message>
        <source>Show bookmarks</source>
        <translation>Yer imlerini göster</translation>
    </message>
    <message>
        <source>Open previous comic</source>
        <translation>Önceki çizgi romanı aç</translation>
    </message>
    <message>
        <source>Rotate image to the left</source>
        <translation>Sayfayı sola yatır</translation>
    </message>
    <message>
        <source>Fit image to height</source>
        <translation>Uygun yüksekliğe getir</translation>
    </message>
    <message>
        <source>Show the bookmarks of the current comic</source>
        <translation>Bu çizgi romanın yer imlerini göster</translation>
    </message>
    <message>
        <source>Show Dictionary</source>
        <translation>Sözlüğü göster</translation>
    </message>
    <message>
        <source>YACReader options</source>
        <translation>YACReader ayarları</translation>
    </message>
    <message>
        <source>Help, About YACReader</source>
        <translation>YACReader hakkında yardım ve bilgi</translation>
    </message>
    <message>
        <source>Show go to flow</source>
        <translation>Akışı göster</translation>
    </message>
    <message>
        <source>Previous Comic</source>
        <translation>Önce ki çizgi roman</translation>
    </message>
    <message>
        <source>Show full size</source>
        <translation>Tam erken</translation>
    </message>
    <message>
        <source>Magnifying glass</source>
        <translation>Büyüteç</translation>
    </message>
    <message>
        <source>Set a bookmark on the current page</source>
        <translation>Sayfayı yer imi olarak ayarla</translation>
    </message>
    <message>
        <source>Do you want to download the new version?</source>
        <translation>Yeni versiyonu indirmek ister misin ?</translation>
    </message>
    <message>
        <source>There was a problem saving YACReader error log file. Please, check if you have enough permissions in the YACReader root folder.</source>
        <translation type="vanished">Kaydederken bir problem çıktı YACReader hata kayıt dosyası. Lütfen YACReader root dosyasını ziyaret edin.</translation>
    </message>
    <message>
        <source>Rotate image to the right</source>
        <translation>Sayfayı sağa yator</translation>
    </message>
    <message>
        <source>Always on top</source>
        <translation>Her zaman üstte</translation>
    </message>
    <message>
        <source>Remind me in 14 days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fit Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <source>Gamma</source>
        <translation>Gama</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Yeniden başlat</translation>
    </message>
    <message>
        <source>My comics path</source>
        <translation>Çizgi Romanlarım</translation>
    </message>
    <message>
        <source>Image adjustment</source>
        <translation>Resim ayarları</translation>
    </message>
    <message>
        <source>Page width stretch</source>
        <translation>Sayfayı uzat</translation>
    </message>
    <message>
        <source>&quot;Go to flow&quot; size</source>
        <translation>Akış görünümüne git</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation>Seç</translation>
    </message>
    <message>
        <source>Image options</source>
        <translation>Sayfa ayarları</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation>Kontrast</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <source>Comics directory</source>
        <translation>Çizgi roman konumu</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Arka plan rengi</translation>
    </message>
    <message>
        <source>Page Flow</source>
        <translation>Sayfa akışı</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation>Parlaklık</translation>
    </message>
    <message>
        <source>Restart is needed</source>
        <translation>Yeniden başlatılmalı</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>7z lib not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unable to load 7z lib from ./utils</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutsDialog</name>
    <message>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <source>YACReader keyboard shortcuts</source>
        <translation>YACReader klavye kısayolları</translation>
    </message>
    <message>
        <source>Keyboard Shortcuts</source>
        <translation>Kılavye Kısayolları</translation>
    </message>
</context>
<context>
    <name>Viewer</name>
    <message>
        <source>Press &apos;O&apos; to open comic.</source>
        <translation>&apos;O&apos;ya basarak aç.</translation>
    </message>
    <message>
        <source>Cover!</source>
        <translation>Kapak!</translation>
    </message>
    <message>
        <source>Comic not found</source>
        <translation>Çizgi roman bulunamadı</translation>
    </message>
    <message>
        <source>Not found</source>
        <translation>Bulunamadı</translation>
    </message>
    <message>
        <source>Last page!</source>
        <translation>Son sayfa!</translation>
    </message>
    <message>
        <source>Loading...please wait!</source>
        <translation>Yükleniyor... lütfen bekleyin!</translation>
    </message>
    <message>
        <source>Error opening comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CRC Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page not available!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderDeletingProgress</name>
    <message>
        <source>cancel</source>
        <translation type="vanished">vazgeç</translation>
    </message>
    <message>
        <source>Please wait, deleting in progress...</source>
        <translation type="vanished">Lütfen bekle silme işlemi gerçekleştiriliyor...</translation>
    </message>
</context>
<context>
    <name>YACReaderFieldEdit</name>
    <message>
        <source>Restore to default</source>
        <translation>Varsayılana ayarla</translation>
    </message>
    <message>
        <source>Click to overwrite</source>
        <translation>Üzerine yazmak için tıkla</translation>
    </message>
</context>
<context>
    <name>YACReaderFieldPlainTextEdit</name>
    <message>
        <source>Restore to default</source>
        <translation>Varsayılana ayarla</translation>
    </message>
    <message>
        <source>Click to overwrite</source>
        <translation>Üstüne yazmak için tıkla</translation>
    </message>
</context>
<context>
    <name>YACReaderFlowConfigWidget</name>
    <message>
        <source>CoverFlow look</source>
        <translation>Kapak akışı görünümü</translation>
    </message>
    <message>
        <source>How to show covers:</source>
        <translation>Kapaklar nasıl gözüksün:</translation>
    </message>
    <message>
        <source>Stripe look</source>
        <translation>Şerit görünüm</translation>
    </message>
    <message>
        <source>Overlapped Stripe look</source>
        <translation>Çakışan şerit görünüm</translation>
    </message>
</context>
<context>
    <name>YACReaderGLFlowConfigWidget</name>
    <message>
        <source>Zoom</source>
        <translation>Yakınlaş</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Işık</translation>
    </message>
    <message>
        <source>Show advanced settings</source>
        <translation>Daha fazla ayar göster</translation>
    </message>
    <message>
        <source>Roulette look</source>
        <translation>Rulet görünüm</translation>
    </message>
    <message>
        <source>Cover Angle</source>
        <translation>Kapak Açısı</translation>
    </message>
    <message>
        <source>Stripe look</source>
        <translation>Strip görünüm</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Pozisyon</translation>
    </message>
    <message>
        <source>Z offset</source>
        <translation>Z dengesi</translation>
    </message>
    <message>
        <source>Y offset</source>
        <translation>Y dengesi</translation>
    </message>
    <message>
        <source>Central gap</source>
        <translation>Boş merkaz</translation>
    </message>
    <message>
        <source>Presets:</source>
        <translation>Hazırlayan:</translation>
    </message>
    <message>
        <source>Overlapped Stripe look</source>
        <translation>Çakışan şerit görünüm</translation>
    </message>
    <message>
        <source>Modern look</source>
        <translation>Modern görünüm</translation>
    </message>
    <message>
        <source>View angle</source>
        <translation>Bakış açısı</translation>
    </message>
    <message>
        <source>Max angle</source>
        <translation>Maksimum açı</translation>
    </message>
    <message>
        <source>Custom:</source>
        <translation>Kişisel:</translation>
    </message>
    <message>
        <source>Classic look</source>
        <translation>Klasik görünüm</translation>
    </message>
    <message>
        <source>Cover gap</source>
        <translation>Kapak </translation>
    </message>
    <message>
        <source>High Performance</source>
        <translation>Yüksek performans</translation>
    </message>
    <message>
        <source>Performance:</source>
        <translation>Performans:</translation>
    </message>
    <message>
        <source>Use VSync (improve the image quality in fullscreen mode, worse performance)</source>
        <translation>VSync kullan</translation>
    </message>
    <message>
        <source>Visibility</source>
        <translation>Görünülebilirlik</translation>
    </message>
    <message>
        <source>Low Performance</source>
        <translation>Düşük Performans</translation>
    </message>
</context>
<context>
    <name>YACReaderOptionsDialog</name>
    <message>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <source>Use hardware acceleration (restart needed)</source>
        <translation>Yüksek donanımlı kullan (yeniden başlatmak gerekli)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Vazgeç</translation>
    </message>
</context>
<context>
    <name>YACReaderSideBar</name>
    <message>
        <source>Search folders and comics</source>
        <translation type="vanished">Dosyaları ve çizgi romanları ara</translation>
    </message>
    <message>
        <source>LIBRARIES</source>
        <translation type="vanished">KÜTÜPHANELER</translation>
    </message>
    <message>
        <source>FOLDERS</source>
        <translation type="vanished">DOSYALAR</translation>
    </message>
</context>
<context>
    <name>YACReaderTranslator</name>
    <message>
        <source>YACReader translator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service not available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
