If you are trying to compile YACReader, you need to donwload de source code of 7zip (Windows) or p7zip (Linux/MacOSX).

Please, extract it and rename the folder to lib7zip (Windows) or libp7zip (Linux/MacOSX), then copy it to $YACREADER_SRC/compressed_archive/ (this 
folder). If you are using a 64 bit Linux-System, please apply libp7zip.patch for successfull compilation or use compileX11.sh.

YACReader is compiled using 7zip/p7zip 9.20.1

