<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_BE">
<context>
    <name>AddLibraryDialog</name>
    <message>
        <location filename="add_library_dialog.cpp" line="17"/>
        <source>Comics folder : </source>
        <translation type="unfinished">Strips map:</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="22"/>
        <source>Library Name : </source>
        <translation type="unfinished">Bibliotheek Naam :</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="27"/>
        <source>Add</source>
        <translation type="unfinished">Toevoegen</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="31"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="67"/>
        <source>Add an existing library</source>
        <translation type="unfinished">Voeg een bestaand bibliotheek toe</translation>
    </message>
</context>
<context>
    <name>ComicVineDialog</name>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="52"/>
        <source>skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="53"/>
        <source>back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="54"/>
        <source>next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="55"/>
        <source>search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="56"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="130"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="142"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="221"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="672"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="706"/>
        <source>Looking for volume...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="140"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="639"/>
        <source>comic %1 of %2 - %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="227"/>
        <source>%1 comics selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="262"/>
        <source>Error connecting to ComicVine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="262"/>
        <source>unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="425"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="465"/>
        <source>Retrieving tags for : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="687"/>
        <source>Retrieving volume info...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="713"/>
        <source>Looking for comic...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateLibraryDialog</name>
    <message>
        <location filename="create_library_dialog.cpp" line="17"/>
        <source>Comics folder : </source>
        <translation type="unfinished">Strips map:</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="22"/>
        <source>Library Name : </source>
        <translation type="unfinished">Bibliotheek Naam :</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="27"/>
        <source>Create</source>
        <translation type="unfinished">Aanmaken</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="31"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="53"/>
        <source>Create a library could take several minutes. You can stop the process and update the library later for completing the task.</source>
        <translation type="unfinished">Een bibliotheek aanmaken kan enkele minuten duren. U kunt het proces stoppen en de bibliotheek later voltooien.</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="75"/>
        <source>Create new library</source>
        <translation type="unfinished">Een nieuwe Bibliotheek aanmaken</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="99"/>
        <source>Path not found</source>
        <translation type="unfinished">Pad niet gevonden </translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="99"/>
        <source>The selected path does not exist or is not a valid path. Be sure that you have write access to this folder</source>
        <translation type="unfinished">De geselecteerde pad bestaat niet of is geen geldig pad. Controleer of u schrijftoegang hebt tot deze map</translation>
    </message>
</context>
<context>
    <name>ExportComicsInfoDialog</name>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="14"/>
        <source>Output file : </source>
        <translation type="unfinished">Uitvoerbestand:</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="18"/>
        <source>Create</source>
        <translation type="unfinished">Aanmaken</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="57"/>
        <source>Export comics info</source>
        <translation type="unfinished">Strip info exporteren </translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="67"/>
        <source>Destination database name</source>
        <translation type="unfinished">Bestemmingsdatabase naam</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="85"/>
        <source>Problem found while writing</source>
        <translation type="unfinished">Probleem bij het schrijven</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="85"/>
        <source>The selected path for the output file does not exist or is not a valid path. Be sure that you have write access to this folder</source>
        <translation type="unfinished">Het gekozen pad voor het uitvoerbestand bestaat niet of is geen geldig pad. Controleer of u schrijftoegang hebt tot deze map</translation>
    </message>
</context>
<context>
    <name>ExportLibraryDialog</name>
    <message>
        <location filename="export_library_dialog.cpp" line="11"/>
        <source>Output folder : </source>
        <translation type="unfinished">Uitvoermap :</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="15"/>
        <source>Create</source>
        <translation type="unfinished">Aanmaken</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="19"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="60"/>
        <source>Create covers package</source>
        <translation type="unfinished">Aanmaken omslag pakket</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="73"/>
        <source>Problem found while writing</source>
        <translation type="unfinished">Probleem bij het schrijven</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="73"/>
        <source>The selected path for the output file does not exist or is not a valid path. Be sure that you have write access to this folder</source>
        <translation type="unfinished">Het gekozen pad voor het uitvoerbestand bestaat niet of is geen geldig pad. Controleer of u schrijftoegang hebt tot deze map</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="79"/>
        <source>Destination directory</source>
        <translation type="unfinished">Doeldirectory</translation>
    </message>
</context>
<context>
    <name>FileComic</name>
    <message>
        <location filename="../common/comic.cpp" line="309"/>
        <source>Unknown error opening the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="418"/>
        <source>7z not found</source>
        <translation type="unfinished">7Z Archiefbestand niet gevonden</translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="424"/>
        <source>Format not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="302"/>
        <source>CRC error on page (%1): some of the pages will not be displayed correctly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpAboutDialog</name>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="21"/>
        <source>About</source>
        <translation type="unfinished">Over</translation>
    </message>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="24"/>
        <source>Help</source>
        <translation type="unfinished">Help</translation>
    </message>
</context>
<context>
    <name>ImportComicsInfoDialog</name>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="14"/>
        <source>Import comics info</source>
        <translation type="unfinished">Strip info Importeren</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="17"/>
        <source>Info database location : </source>
        <translation type="unfinished">Info database locatie :</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="21"/>
        <source>Import</source>
        <translation type="unfinished">Importeren</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="25"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="77"/>
        <source>Comics info file (*.ydb)</source>
        <translation type="unfinished">Strips info bestand ( * .ydb)</translation>
    </message>
</context>
<context>
    <name>ImportLibraryDialog</name>
    <message>
        <location filename="import_library_dialog.cpp" line="17"/>
        <source>Library Name : </source>
        <translation type="unfinished">Bibliotheek Naam :</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="22"/>
        <source>Package location : </source>
        <translation type="unfinished">Arrangement locatie :</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="26"/>
        <source>Destination folder : </source>
        <translation type="unfinished">Doelmap:</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="30"/>
        <source>Unpack</source>
        <translation type="unfinished">Uitpakken</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="34"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="87"/>
        <source>Extract a catalog</source>
        <translation type="unfinished">Een catalogus uitpakken</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="111"/>
        <source>Compresed library covers (*.clc)</source>
        <translation type="unfinished">Compresed omslag- bibliotheek ( * .clc)</translation>
    </message>
</context>
<context>
    <name>ImportWidget</name>
    <message>
        <location filename="import_widget.cpp" line="151"/>
        <source>stop</source>
        <translation type="unfinished">stop</translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="190"/>
        <source>Some of the comics being added...</source>
        <translation type="unfinished">Enkele strips zijn toegevoegd ...</translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="357"/>
        <source>Importing comics</source>
        <translation type="unfinished">Strips importeren</translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="358"/>
        <source>&lt;p&gt;YACReaderLibrary is now creating a new library.&lt;/p&gt;&lt;p&gt;Create a library could take several minutes. You can stop the process and update the library later for completing the task.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;P&gt;YACReaderLibrary maak nu een nieuwe bibliotheek. &lt; /p&gt; &lt;p&gt;Een bibliotheek aanmaken kan enkele minuten duren. U kunt het proces stoppen en de bibliotheek later voltooien. &lt; /p&gt;</translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="364"/>
        <source>Updating the library</source>
        <translation type="unfinished">Actualisering van de bibliotheek</translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="365"/>
        <source>&lt;p&gt;The current library is being updated. For faster updates, please, update your libraries frequently.&lt;/p&gt;&lt;p&gt;You can stop the process and continue updating this library later.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;P&gt;De huidige bibliotheek wordt bijgewerkt. Voor snellere updates, update uw bibliotheken regelmatig. &lt; /p&gt; &lt;p&gt;u kunt het proces stoppen om later bij te werken. &lt; /p&gt;</translation>
    </message>
</context>
<context>
    <name>LibraryWindow</name>
    <message>
        <location filename="library_window.cpp" line="111"/>
        <source>YACReader Library</source>
        <translation type="unfinished">YACReader Bibliotheek</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="146"/>
        <location filename="library_window.cpp" line="689"/>
        <source>Library</source>
        <translation type="unfinished">Bibliotheek</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="177"/>
        <source>&lt;font color=&apos;white&apos;&gt; press &apos;F&apos; to close fullscreen mode &lt;/font&gt;</source>
        <translation type="unfinished">&lt;font color=&apos;white&apos;&gt;Druk op &quot;F&quot; om &apos;volledig scherm modus&apos; te sluiten &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="332"/>
        <source>Create a new library</source>
        <translation type="unfinished">Maak een nieuwe Bibliotheek </translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="337"/>
        <source>Open an existing library</source>
        <translation type="unfinished">Open een bestaande Bibliotheek</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="341"/>
        <location filename="library_window.cpp" line="342"/>
        <source>Export comics info</source>
        <translation type="unfinished">Exporteren van strip info</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="345"/>
        <location filename="library_window.cpp" line="346"/>
        <source>Import comics info</source>
        <translation type="unfinished">Importeren van strip info</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="349"/>
        <source>Pack covers</source>
        <translation type="unfinished">Inpakken strip voorbladen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="350"/>
        <source>Pack the covers of the selected library</source>
        <translation type="unfinished">Inpakken alle strip voorbladen  van de geselecteerde Bibliotheek</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="353"/>
        <source>Unpack covers</source>
        <translation type="unfinished">Uitpakken voorbladen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="354"/>
        <source>Unpack a catalog</source>
        <translation type="unfinished">Uitpaken van een catalogus</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="357"/>
        <source>Update library</source>
        <translation type="unfinished">Bibliotheek bijwerken</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="358"/>
        <source>Update current library</source>
        <translation type="unfinished">Huidige Bibliotheek bijwerken</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="362"/>
        <source>Rename library</source>
        <translation type="unfinished">Bibliotheek hernoemen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="363"/>
        <source>Rename current library</source>
        <translation type="unfinished">Huidige Bibliotheek hernoemen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="367"/>
        <source>Remove library</source>
        <translation type="unfinished">Bibliotheek verwijderen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="368"/>
        <source>Remove current library from your collection</source>
        <translation type="unfinished">De huidige Bibliotheek verwijderen uit uw verzameling</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="371"/>
        <source>Open current comic</source>
        <translation type="unfinished">Huidige strip openen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="372"/>
        <source>Open current comic on YACReader</source>
        <translation type="unfinished">Huidige strip openen in YACReader</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="376"/>
        <location filename="library_window.cpp" line="457"/>
        <source>Set as read</source>
        <translation type="unfinished">Instellen als gelezen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="377"/>
        <source>Set comic as read</source>
        <translation type="unfinished">Strip Instellen als gelezen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="380"/>
        <location filename="library_window.cpp" line="461"/>
        <source>Set as unread</source>
        <translation type="unfinished">Instellen als ongelezen </translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="381"/>
        <source>Set comic as unread</source>
        <translation type="unfinished">Strip Instellen als ongelezen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="392"/>
        <source>Show/Hide marks</source>
        <translation type="unfinished">Toon/Verberg markeringen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="393"/>
        <source>Show or hide readed marks</source>
        <translation type="unfinished">Toon of Verberg gelezen markeringen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="958"/>
        <source>Library not available</source>
        <oldsource>Library &apos;</oldsource>
        <translation type="unfinished">Bibliotheek niet beschikbaar </translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="399"/>
        <source>Fullscreen mode on/off</source>
        <translation type="unfinished">Volledig scherm modus aan/of</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="400"/>
        <source>Fullscreen mode on/off (F)</source>
        <translation type="unfinished">Volledig scherm modus aan/of (F)</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="407"/>
        <source>Help, About YACReader</source>
        <translation type="unfinished">Help, Over YACReader</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="415"/>
        <source>Select root node</source>
        <translation type="unfinished">Selecteer de hoofd categorie</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="419"/>
        <source>+</source>
        <translation type="unfinished">+</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="420"/>
        <source>Expand all nodes</source>
        <translation type="unfinished">Alle categorieën uitklappen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="424"/>
        <source>-</source>
        <translation type="unfinished">-</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="425"/>
        <source>Colapse all nodes</source>
        <translation type="unfinished">Alle categorieën inklappen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="430"/>
        <source>Show options dialog</source>
        <translation type="unfinished">Toon opties dialoog</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="437"/>
        <source>Show comics server options dialog</source>
        <translation type="unfinished">Toon strips-server opties dialoog</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="445"/>
        <source>Open folder...</source>
        <translation type="unfinished">Map openen ...</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="449"/>
        <source>Set as uncompleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="453"/>
        <source>Set as completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="465"/>
        <source>Open containing folder...</source>
        <translation type="unfinished">Open map ...</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="469"/>
        <source>Reset comic rating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="473"/>
        <source>Select all comics</source>
        <translation type="unfinished">Selecteer alle strips</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="477"/>
        <source>Edit</source>
        <translation type="unfinished">Bewerken</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="481"/>
        <source>Asign current order to comics</source>
        <translation type="unfinished">Nummeren van strips</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="485"/>
        <source>Update cover</source>
        <translation type="unfinished">Strip omslagen bijwerken</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="489"/>
        <source>Delete selected comics</source>
        <translation type="unfinished">Geselecteerde strips verwijderen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="493"/>
        <source>Hide comic flow</source>
        <translation type="unfinished">Sluit de Omslagbrowser</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="499"/>
        <source>Download tags from Comic Vine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="705"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="715"/>
        <source>Comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="875"/>
        <source>Update needed</source>
        <translation type="unfinished">Bijwerken is nodig</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="875"/>
        <source>This library was created with a previous version of YACReaderLibrary. It needs to be updated. Update now?</source>
        <translation type="unfinished">Deze bibliotheek is gemaakt met een vorige versie van YACReaderLibrary. Het moet worden bijgewerkt. Nu bijwerken?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="881"/>
        <source>Update failed</source>
        <translation type="unfinished">Bijwerken mislukt</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="881"/>
        <source>The current library can&apos;t be udpated. Check for write write permissions on: </source>
        <translation type="unfinished">De huidige bibliotheek kan niet worden bijgewerkt. Controleer bij of u schrijfbevoegdheid hebt:</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="934"/>
        <source>Download new version</source>
        <translation type="unfinished">Nieuwe versie ophalen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="934"/>
        <source>This library was created with a newer version of YACReaderLibrary. Download the new version now?</source>
        <translation type="unfinished">Deze bibliotheek is gemaakt met een nieuwere versie van YACReaderLibrary. Download de nieuwe versie?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="958"/>
        <source>Library &apos;%1&apos; is no longer available. Do you want to remove it?</source>
        <translation type="unfinished">Bibliotheek &apos; %1&apos; is niet langer beschikbaar. Wilt u het verwijderen?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="982"/>
        <source>Old library</source>
        <translation type="unfinished">Oude Bibliotheek </translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="982"/>
        <source>Library &apos;%1&apos; has been created with an older version of YACReaderLibrary. It must be created again. Do you want to create the library now?</source>
        <translation type="unfinished">Bibliotheek &apos; %1&apos; is gemaakt met een oudere versie van YACReaderLibrary. Zij moet opnieuw worden aangemaakt. Wilt u de bibliotheek nu aanmaken?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1156"/>
        <source>YACReader not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1156"/>
        <source>YACReader not found, YACReader should be installed in the same folder as YACReaderLibrary.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1255"/>
        <source>Library not found</source>
        <translation type="unfinished">Bibliotheek niet gevonden</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1255"/>
        <source>The selected folder doesn&apos;t contain any library.</source>
        <translation type="unfinished">De geselecteerde map bevat geen bibliotheek.</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1328"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Weet u het zeker?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1328"/>
        <source>Do you want remove </source>
        <translation type="unfinished">Wilt u verwijderen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1328"/>
        <source> library?</source>
        <translation type="unfinished">Bibliotheek?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1329"/>
        <source>Remove and delete metadata</source>
        <translation type="unfinished">Verwijder  metagegevens </translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1544"/>
        <source>Unable to delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1544"/>
        <source>There was an issue trying to delete the selected comics. Please, check for write permissions in the selected files or containing folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1569"/>
        <source>Asign comics numbers</source>
        <translation type="unfinished">Strips nummeren</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1570"/>
        <source>Asign numbers starting in:</source>
        <translation type="unfinished">Strips nummeren beginnen bij:</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1733"/>
        <source>Error creating the library</source>
        <translation type="unfinished">Fout bij aanmaken Bibliotheek</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1738"/>
        <source>Error updating the library</source>
        <translation type="unfinished">Fout bij bijwerken Bibliotheek</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1743"/>
        <source>Error opening the library</source>
        <translation type="unfinished">Fout bij openen Bibliotheek</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1768"/>
        <source>Delete comics</source>
        <translation type="unfinished">Strips verwijderen</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1768"/>
        <source>All the selected comics will be deleted from your disk. Are you sure?</source>
        <translation type="unfinished">Alle geselecteerde strips worden verwijderd van uw schijf. Weet u het zeker?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1887"/>
        <source>Library name already exists</source>
        <translation type="unfinished">Bibliotheek naam bestaat al</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1887"/>
        <source>There is another library with the name &apos;%1&apos;.</source>
        <translation type="unfinished">Er is al een bibliotheek met de naam &apos; %1 &apos;.</translation>
    </message>
</context>
<context>
    <name>LocalComicListModel</name>
    <message>
        <location filename="comic_vine/model/local_comic_list_model.cpp" line="75"/>
        <source>file name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoLibrariesWidget</name>
    <message>
        <location filename="no_libraries_widget.cpp" line="26"/>
        <source>You don&apos;t have any librarires yet</source>
        <translation type="unfinished">Je hebt geen nog librarires</translation>
    </message>
    <message>
        <location filename="no_libraries_widget.cpp" line="28"/>
        <source>&lt;p&gt;You can create a library in any folder, YACReaderLibrary will import all comics and folders from this folder. If you have created any library in the past you can open them.&lt;/p&gt;&lt;p&gt;Don&apos;t forget that you can use YACReader as a stand alone application for reading the comics on your computer.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;P&gt;u kunt een bibliotheek maken in een willekeurige map, YACReaderLibrary importeert alle strips en mappen uit deze map. Alle bibliotheek aangemaakt in het verleden kan je openen. &lt; /p&gt; &lt;p&gt;vergeet niet dat u YACReader kan gebruiken als stand-alone applicatie voor het lezen van de strips op de computer. &lt; /p&gt;</translation>
    </message>
    <message>
        <location filename="no_libraries_widget.cpp" line="32"/>
        <source>create your first library</source>
        <translation type="unfinished">Maak uw eerste bibliotheek</translation>
    </message>
    <message>
        <location filename="no_libraries_widget.cpp" line="34"/>
        <source>add an existing one</source>
        <translation type="unfinished">voeg een bestaande bibliotheek toe</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="options_dialog.cpp" line="47"/>
        <source>Options</source>
        <translation type="unfinished">Opties</translation>
    </message>
</context>
<context>
    <name>PropertiesDialog</name>
    <message>
        <location filename="properties_dialog.cpp" line="74"/>
        <source>General info</source>
        <translation type="unfinished">Algemene Info</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="75"/>
        <source>Authors</source>
        <translation type="unfinished">Auteurs</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="76"/>
        <source>Publishing</source>
        <translation type="unfinished">Uitgever</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="77"/>
        <source>Plot</source>
        <translation type="unfinished">Verhaal</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="86"/>
        <source>Cover page</source>
        <translation type="unfinished">Omslag</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="147"/>
        <source>Title:</source>
        <translation type="unfinished">Titel:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="163"/>
        <source>Issue number:</source>
        <translation type="unfinished">Ids:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="165"/>
        <source>Volume:</source>
        <translation type="unfinished">Inhoud:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="178"/>
        <source>Story arc:</source>
        <translation type="unfinished">Verhaallijn:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="180"/>
        <source>Genere:</source>
        <translation type="unfinished">Genre:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="182"/>
        <source>Size:</source>
        <translation type="unfinished">Grootte(MB):</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="206"/>
        <source>Writer(s):</source>
        <translation type="unfinished">Schrijver(s):</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="209"/>
        <source>Penciller(s):</source>
        <translation type="unfinished">Tekenaar(s):</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="217"/>
        <source>Inker(s):</source>
        <translation type="unfinished">Inker(s):</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="220"/>
        <source>Colorist(s):</source>
        <translation type="unfinished">Inkleurder(s):</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="230"/>
        <source>Letterer(s):</source>
        <translation type="unfinished">Letterzetter(s):</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="233"/>
        <source>Cover Artist(s):</source>
        <translation type="unfinished">Omslag ontwikkelaar(s):</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="256"/>
        <source>Day:</source>
        <translation type="unfinished">Dag:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="260"/>
        <source>Month:</source>
        <translation type="unfinished">Maand:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="264"/>
        <source>Year:</source>
        <translation type="unfinished">Jaar:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="272"/>
        <source>Publisher:</source>
        <translation type="unfinished">Uitgever:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="273"/>
        <source>Format:</source>
        <translation type="unfinished">Formaat:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="274"/>
        <source>Color/BW:</source>
        <translation type="unfinished">Kleur/ZW:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="275"/>
        <source>Age rating:</source>
        <translation type="unfinished">Leeftijdsbeperking:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="288"/>
        <source>Synopsis:</source>
        <translation type="unfinished">Synopsis:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="289"/>
        <source>Characters:</source>
        <translation type="unfinished">Personages:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="290"/>
        <source>Notes:</source>
        <translation type="unfinished">Opmerkingen:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="388"/>
        <source>Comic Vine link: &lt;a style=&apos;color: #FFCB00; text-decoration:none; font-weight:bold;&apos; href=&quot;http://www.comicvine.com/comic/4000-%1/&quot;&gt; view &lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="416"/>
        <source>Not found</source>
        <translation type="unfinished">Niet gevonden</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="416"/>
        <source>Comic not found. You should update your library.</source>
        <translation type="unfinished">Strip niet gevonden. U moet uw bibliotheek.bijwerken.</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="491"/>
        <source>Edit selected comics information</source>
        <translation type="unfinished">Geselecteerde strip informatie bijwerken</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="562"/>
        <source>Edit comic information</source>
        <translation type="unfinished">Strip informatie bijwerken</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>7z lib not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>unable to load 7z lib from ./utils</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="rename_library_dialog.cpp" line="17"/>
        <source>New Library Name : </source>
        <translation type="unfinished">Nieuwe Bibliotheek Naam :</translation>
    </message>
    <message>
        <location filename="rename_library_dialog.cpp" line="22"/>
        <source>Rename</source>
        <translation type="unfinished">Hernoem</translation>
    </message>
    <message>
        <location filename="rename_library_dialog.cpp" line="26"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="rename_library_dialog.cpp" line="54"/>
        <source>Rename current library</source>
        <translation type="unfinished">Hernoem de huidige bibiliotheek</translation>
    </message>
</context>
<context>
    <name>ScraperResultsPaginator</name>
    <message>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="32"/>
        <source>Number of volumes found : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="34"/>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="57"/>
        <source>page %1 of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="56"/>
        <source>Number of %1 found : %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchSingleComic</name>
    <message>
        <location filename="comic_vine/search_single_comic.cpp" line="14"/>
        <source>Please provide some additional information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/search_single_comic.cpp" line="19"/>
        <source>Series:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchVolume</name>
    <message>
        <location filename="comic_vine/search_volume.cpp" line="11"/>
        <source>Please provide some additional information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/search_volume.cpp" line="14"/>
        <source>Series:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectComic</name>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="17"/>
        <source>Please, select the right comic info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="40"/>
        <source>comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="99"/>
        <source>loading cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="100"/>
        <source>loading description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="143"/>
        <source>description unavailable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectVolume</name>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="32"/>
        <source>Please, select the right series for your comic.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="63"/>
        <source>volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="128"/>
        <source>loading cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="129"/>
        <source>loading description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="172"/>
        <source>description unavailable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeriesQuestion</name>
    <message>
        <location filename="comic_vine/series_question.cpp" line="13"/>
        <source>You are trying to get information for various comics at once, are they part of the same series?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/series_question.cpp" line="15"/>
        <source>yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location filename="comic_vine/series_question.cpp" line="16"/>
        <source>no</source>
        <translation type="unfinished">neen</translation>
    </message>
</context>
<context>
    <name>ServerConfigDialog</name>
    <message>
        <location filename="server_config_dialog.cpp" line="61"/>
        <source>set port</source>
        <translation type="unfinished">Poort instellen</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="68"/>
        <source>EASY SERVER CONNECTION</source>
        <translation type="unfinished">GEMAKKELIJKE VERBINDING MET DE SERVER</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="72"/>
        <source>SERVER ADDRESS</source>
        <translation type="unfinished">SERVERADRES</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="76"/>
        <source>just scan the code with your device!!</source>
        <translation type="unfinished">Scan de code met uw apparaat!</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="82"/>
        <source>YACReader is now available for iOS devices, the best comic reading experience now in your iPad, iPhone or iPod touch. &lt;a href=&apos;http://ios.yacreader.com&apos; style=&apos;color:rgb(193, 148, 65)&apos;&gt; Discover it! &lt;/a&gt;</source>
        <translation type="unfinished">YACReader is nu beschikbaar voor iOS apparaten, de beste strip leeservaring nu op uw iPad, iPhone of iPod touch. &lt;A href= &quot;http://ios.yacreader.com&apos; style= &quot;color:rgb(193, 148, 65) &quot;&gt; Ontdek het zelf! &lt; /A&gt;</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="128"/>
        <source>IP address</source>
        <translation type="unfinished">IP-adres</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="129"/>
        <source>Port</source>
        <translation type="unfinished">Poort</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="142"/>
        <source>enable the server</source>
        <translation type="unfinished">De server instellen</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="294"/>
        <source>QR generator error!</source>
        <translation type="unfinished">QR generator fout!</translation>
    </message>
</context>
<context>
    <name>SortVolumeComics</name>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="18"/>
        <source>Please, sort the list of comics on the left until it matches the comics&apos; information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="21"/>
        <source>sort comics to match comic information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="62"/>
        <source>issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="91"/>
        <source>remove selected comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="92"/>
        <source>restore all removed comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="93"/>
        <source>restore removed comics</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableModel</name>
    <message>
        <location filename="db/tablemodel.cpp" line="94"/>
        <source>yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="94"/>
        <source>no</source>
        <translation type="unfinished">neen</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="127"/>
        <source>Title</source>
        <translation type="unfinished">Titel</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="129"/>
        <source>File Name</source>
        <translation type="unfinished">Bestandsnaam</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="131"/>
        <source>Pages</source>
        <translation type="unfinished">Pagina&apos;s</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="133"/>
        <source>Size</source>
        <translation type="unfinished">Grootte(MB)</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="135"/>
        <source>Read</source>
        <translation type="unfinished">Gelezen</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="137"/>
        <source>Current Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="139"/>
        <source>Rating</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleHeader</name>
    <message>
        <location filename="comic_vine/title_header.cpp" line="30"/>
        <source>SEARCH</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateLibraryDialog</name>
    <message>
        <location filename="create_library_dialog.cpp" line="166"/>
        <source>Updating....</source>
        <translation type="unfinished">Bijwerken....</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="172"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="191"/>
        <source>Update library</source>
        <translation type="unfinished">Bibliotheek bijwerken</translation>
    </message>
</context>
<context>
    <name>VolumeComicsModel</name>
    <message>
        <location filename="comic_vine/model/volume_comics_model.cpp" line="122"/>
        <source>title</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumesModel</name>
    <message>
        <location filename="comic_vine/model/volumes_model.cpp" line="112"/>
        <source>year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/model/volumes_model.cpp" line="114"/>
        <source>issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/model/volumes_model.cpp" line="116"/>
        <source>publisher</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderDeletingProgress</name>
    <message>
        <location filename="../custom_widgets/yacreader_deleting_progress.cpp" line="20"/>
        <source>Please wait, deleting in progress...</source>
        <translation type="unfinished">Even geduld, verwijderen ...</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_deleting_progress.cpp" line="35"/>
        <source>cancel</source>
        <translation type="unfinished">annuleren</translation>
    </message>
</context>
<context>
    <name>YACReaderFieldEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="29"/>
        <source>Click to overwrite</source>
        <translation type="unfinished">Klik hier om te overschrijven</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="11"/>
        <source>Restore to default</source>
        <translation type="unfinished">Standaardwaarden herstellen</translation>
    </message>
</context>
<context>
    <name>YACReaderFieldPlainTextEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="20"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="45"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="51"/>
        <source>Click to overwrite</source>
        <translation type="unfinished">Klik hier om te overschrijven</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="10"/>
        <source>Restore to default</source>
        <translation type="unfinished">Standaardwaarden herstellen</translation>
    </message>
</context>
<context>
    <name>YACReaderFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="13"/>
        <source>How to show covers:</source>
        <translation type="unfinished">Hoe omslagbladen bekijken:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="15"/>
        <source>CoverFlow look</source>
        <translation type="unfinished">Omslagbrowser uiterlijk</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="16"/>
        <source>Stripe look</source>
        <translation type="unfinished">Brede band</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="17"/>
        <source>Overlapped Stripe look</source>
        <translation type="unfinished">Overlappende band</translation>
    </message>
</context>
<context>
    <name>YACReaderGLFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="19"/>
        <source>Presets:</source>
        <translation type="unfinished">Voorinstellingen:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="21"/>
        <source>Classic look</source>
        <translation type="unfinished">Klassiek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="24"/>
        <source>Stripe look</source>
        <translation type="unfinished">Brede band</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="27"/>
        <source>Overlapped Stripe look</source>
        <translation type="unfinished">Overlappende band</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="30"/>
        <source>Modern look</source>
        <translation type="unfinished">Modern</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="33"/>
        <source>Roulette look</source>
        <translation type="unfinished">Roulette</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="77"/>
        <source>Show advanced settings</source>
        <translation type="unfinished">Toon geavanceerde instellingen</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="86"/>
        <source>Custom:</source>
        <translation type="unfinished">Aangepast:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="89"/>
        <source>View angle</source>
        <translation type="unfinished">Kijkhoek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="95"/>
        <source>Position</source>
        <translation type="unfinished">Positie</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="101"/>
        <source>Cover gap</source>
        <translation type="unfinished">Ruimte tss Omslag</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="107"/>
        <source>Central gap</source>
        <translation type="unfinished">Centrale ruimte</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="113"/>
        <source>Zoom</source>
        <translation type="unfinished">Zoom</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="119"/>
        <source>Y offset</source>
        <translation type="unfinished">Y-positie</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="125"/>
        <source>Z offset</source>
        <translation type="unfinished">Z- positie</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="131"/>
        <source>Cover Angle</source>
        <translation type="unfinished">Omslag hoek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="137"/>
        <source>Visibility</source>
        <translation type="unfinished">Zichtbaarheid</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="143"/>
        <source>Light</source>
        <translation type="unfinished">Licht</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="149"/>
        <source>Max angle</source>
        <translation type="unfinished">Maximale hoek</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="181"/>
        <source>Low Performance</source>
        <translation type="unfinished">Lage Prestaties</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="183"/>
        <source>High Performance</source>
        <translation type="unfinished">Hoge Prestaties</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="194"/>
        <source>Use VSync (improve the image quality in fullscreen mode, worse performance)</source>
        <translation type="unfinished">Gebruik VSync (verbetering van de beeldkwaliteit in de modus volledig scherm, slechtere prestatie)</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="202"/>
        <source>Performance:</source>
        <translation type="unfinished">Prestatie:</translation>
    </message>
</context>
<context>
    <name>YACReaderOptionsDialog</name>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="21"/>
        <source>Save</source>
        <translation type="unfinished">Bewaar</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuleren</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="30"/>
        <source>Use hardware acceleration (restart needed)</source>
        <translation type="unfinished">Gebruik hardware versnelling (opnieuw opstarten vereist)</translation>
    </message>
</context>
<context>
    <name>YACReaderSideBar</name>
    <message>
        <location filename="../custom_widgets/yacreader_sidebar.cpp" line="21"/>
        <source>LIBRARIES</source>
        <translation type="unfinished">BIBLIOTHEKEN</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_sidebar.cpp" line="23"/>
        <source>FOLDERS</source>
        <translation type="unfinished">MAPPEN</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_sidebar.cpp" line="30"/>
        <source>Search folders and comics</source>
        <translation type="unfinished">Zoeken in mappen en strips</translation>
    </message>
</context>
</TS>
