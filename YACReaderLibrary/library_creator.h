#ifndef __LIBRARY_CREATOR_H
#define __LIBRARY_CREATOR_H

#include <QObject>
#include <QString>
#include <QDir>
#include <QFile>
#include <QByteArray>
#include <QRegExp>
#include <QProcess>
#include <QtCore>
#include <QtGui>
#include <QMutex>
#include <QThread>
#include <QSqlDatabase>

#include "folder.h"
#include "comic_db.h"


	class LibraryCreator : public QThread
	{
		Q_OBJECT
	public:
		LibraryCreator();
		void createLibrary(const QString & source, const QString & target);
				void updateLibrary(const QString & source, const QString & target);
				void stop();
	private:
		void processLibrary(const QString & source, const QString & target);
		enum Mode {CREATOR,UPDATER};
		//atributos "globales" durante el proceso de creaci�n y actualizaci�n
		enum Mode _mode;
		QString _source;
		QString _target;
		QStringList _nameFilter;
		QSqlDatabase _database;
		QList<Folder> _currentPathFolders; //lista de folders en el orden en el que est�n siendo explorados, el �ltimo es el folder actual
		//recursive method
		void create(QDir currentDirectory);
		void update(QDir currentDirectory);
		void run();
		qulonglong insertFolders();//devuelve el id del �ltimo folder a�adido (�ltimo en la ruta)
		bool checkCover(const QString & hash);
		void insertComic(const QString & relativePath,const QFileInfo & fileInfo);
		//qulonglong insertFolder(qulonglong parentId,const Folder & folder);
		//qulonglong insertComic(const Comic & comic);
		bool stopRunning;
		//LibraryCreator est� en modo creaci�n si creation == true;
		bool creation;
	signals:
		void finished();
		void coverExtracted(QString);
		void folderUpdated(QString);
		void comicAdded(QString,QString);
		void updated();
		void created();
		void failedCreatingDB(QString);
		void failedOpeningDB(QString);
	};

	class ThumbnailCreator : public QObject
	{
		Q_OBJECT

	public:
		ThumbnailCreator(QString fileSource, QString target="", int coverPage = 1);
	private:
		QString _fileSource;
		QString _target;
		QString _currentName;
		int _numPages;
		QPixmap _cover;
		int _coverPage;
		static bool crash;

	public slots:
		void create();		
		int getNumPages(){return _numPages;};
		QPixmap getCover(){return _cover;};
	signals:
		void openingError(QProcess::ProcessError error);

	};

#endif
