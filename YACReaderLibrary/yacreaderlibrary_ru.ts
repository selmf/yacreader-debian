<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AddLibraryDialog</name>
    <message>
        <location filename="add_library_dialog.cpp" line="17"/>
        <source>Comics folder : </source>
        <translation>Папка комиксов:</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="22"/>
        <source>Library Name : </source>
        <translation>Имя библиотеки:</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="27"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="31"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="add_library_dialog.cpp" line="67"/>
        <source>Add an existing library</source>
        <translation>Добавить в существующую библиотеку</translation>
    </message>
</context>
<context>
    <name>ComicVineDialog</name>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="52"/>
        <source>skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="53"/>
        <source>back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="54"/>
        <source>next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="55"/>
        <source>search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="56"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="130"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="142"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="221"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="672"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="706"/>
        <source>Looking for volume...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="140"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="639"/>
        <source>comic %1 of %2 - %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="227"/>
        <source>%1 comics selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="262"/>
        <source>Error connecting to ComicVine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="262"/>
        <source>unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="425"/>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="465"/>
        <source>Retrieving tags for : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="687"/>
        <source>Retrieving volume info...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/comic_vine_dialog.cpp" line="713"/>
        <source>Looking for comic...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateLibraryDialog</name>
    <message>
        <location filename="create_library_dialog.cpp" line="17"/>
        <source>Comics folder : </source>
        <translation>Папка комиксов:</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="22"/>
        <source>Library Name : </source>
        <translation>Имя библиотеки:</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="27"/>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="31"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="53"/>
        <source>Create a library could take several minutes. You can stop the process and update the library later for completing the task.</source>
        <translation>Создание библиотеки может занять несколько минут. Вы можете остановить процесс и обновить библиотеку позже для завершения задачи.</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="75"/>
        <source>Create new library</source>
        <translation>Создать новую библиотеку</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="99"/>
        <source>Path not found</source>
        <translation>Путь не найден</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="99"/>
        <source>The selected path does not exist or is not a valid path. Be sure that you have write access to this folder</source>
        <translation>Выбранный путь отсутствует, либо неверен. Убедитесь , что у вас есть доступ к этой папке</translation>
    </message>
</context>
<context>
    <name>ExportComicsInfoDialog</name>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="14"/>
        <source>Output file : </source>
        <translation>Файл вывода:</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="18"/>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="57"/>
        <source>Export comics info</source>
        <translation>Экспортировать информацию комикса</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="67"/>
        <source>Destination database name</source>
        <translation>Имя назначенной базы данных</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="85"/>
        <source>Problem found while writing</source>
        <translation>Проблема при написании</translation>
    </message>
    <message>
        <location filename="export_comics_info_dialog.cpp" line="85"/>
        <source>The selected path for the output file does not exist or is not a valid path. Be sure that you have write access to this folder</source>
        <translation>Выбранный путь для импортируемого файла отсутствует, либо неверен. Убедитесь , что у вас есть доступ к этой папке</translation>
    </message>
</context>
<context>
    <name>ExportLibraryDialog</name>
    <message>
        <location filename="export_library_dialog.cpp" line="11"/>
        <source>Output folder : </source>
        <translation>Файл вывода:</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="15"/>
        <source>Create</source>
        <translation>Создать</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="19"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="60"/>
        <source>Create covers package</source>
        <translation>Создать комплект обложек</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="73"/>
        <source>Problem found while writing</source>
        <translation>Проблема при написании</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="73"/>
        <source>The selected path for the output file does not exist or is not a valid path. Be sure that you have write access to this folder</source>
        <translation>Выбранный путь для импортируемого файла отсутствует, либо неверен. Убедитесь , что у вас есть доступ к этой папке</translation>
    </message>
    <message>
        <location filename="export_library_dialog.cpp" line="79"/>
        <source>Destination directory</source>
        <translation>Назначенное местонахождение</translation>
    </message>
</context>
<context>
    <name>FileComic</name>
    <message>
        <location filename="../common/comic.cpp" line="309"/>
        <source>Unknown error opening the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="418"/>
        <source>7z not found</source>
        <translation type="unfinished">7z не найден</translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="424"/>
        <source>Format not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/comic.cpp" line="302"/>
        <source>CRC error on page (%1): some of the pages will not be displayed correctly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpAboutDialog</name>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="21"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../custom_widgets/help_about_dialog.cpp" line="24"/>
        <source>Help</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>ImportComicsInfoDialog</name>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="14"/>
        <source>Import comics info</source>
        <translation>Импортировать информаию комикса</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="17"/>
        <source>Info database location : </source>
        <translation>Местонахождение базы данных:</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="21"/>
        <source>Import</source>
        <translation>Импортировать</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="25"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="import_comics_info_dialog.cpp" line="77"/>
        <source>Comics info file (*.ydb)</source>
        <translation>Информация файла комикса</translation>
    </message>
</context>
<context>
    <name>ImportLibraryDialog</name>
    <message>
        <location filename="import_library_dialog.cpp" line="17"/>
        <source>Library Name : </source>
        <translation>Имя библиотеки:</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="22"/>
        <source>Package location : </source>
        <translation>Местоположение комплекта:</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="26"/>
        <source>Destination folder : </source>
        <translation>Назначенная папка:</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="30"/>
        <source>Unpack</source>
        <translation>Распаковать</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="87"/>
        <source>Extract a catalog</source>
        <translation>Извлечь каталог</translation>
    </message>
    <message>
        <location filename="import_library_dialog.cpp" line="111"/>
        <source>Compresed library covers (*.clc)</source>
        <translation>Сжатая библиотека обложек</translation>
    </message>
</context>
<context>
    <name>ImportWidget</name>
    <message>
        <location filename="import_widget.cpp" line="357"/>
        <source>Importing comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="151"/>
        <source>stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="190"/>
        <source>Some of the comics being added...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="358"/>
        <source>&lt;p&gt;YACReaderLibrary is now creating a new library.&lt;/p&gt;&lt;p&gt;Create a library could take several minutes. You can stop the process and update the library later for completing the task.&lt;/p&gt;</source>
        <oldsource>Create a library could take several minutes. You can stop the process and update the library later for completing the task.</oldsource>
        <translation type="unfinished">Создание библиотеки может занять несколько минут. Вы можете остановить процесс и обновить библиотеку позже для завершения задачи.</translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="364"/>
        <source>Updating the library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="import_widget.cpp" line="365"/>
        <source>&lt;p&gt;The current library is being updated. For faster updates, please, update your libraries frequently.&lt;/p&gt;&lt;p&gt;You can stop the process and continue updating this library later.&lt;/p&gt;</source>
        <oldsource>&lt;p&gt;The current library is being updated. For faster updates, please, update your libraries frequently.&lt;/p&gt;&lt;p&gt;You can stop the process and continue updating this library later.</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryWindow</name>
    <message>
        <location filename="library_window.cpp" line="111"/>
        <source>YACReader Library</source>
        <translation>Библиотека YACReader</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="177"/>
        <source>&lt;font color=&apos;white&apos;&gt; press &apos;F&apos; to close fullscreen mode &lt;/font&gt;</source>
        <translation>&lt;font color=&apos;white&apos;&gt; нажмите &apos;F&apos; чтобы выйте из Полноэкранного режима &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="332"/>
        <source>Create a new library</source>
        <translation>Создать новую библиотеку</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="337"/>
        <source>Open an existing library</source>
        <translation>Открыть существующую библиотеку</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="341"/>
        <location filename="library_window.cpp" line="342"/>
        <source>Export comics info</source>
        <translation>Експорт комикса</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="345"/>
        <location filename="library_window.cpp" line="346"/>
        <source>Import comics info</source>
        <translation>Импорт комикса</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="349"/>
        <source>Pack covers</source>
        <translation>Запакавать обложки</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="350"/>
        <source>Pack the covers of the selected library</source>
        <translation>Запакавать обложки выбранной библиотеки</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="353"/>
        <source>Unpack covers</source>
        <translation>Распокавать обложки</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="354"/>
        <source>Unpack a catalog</source>
        <translation>Распакавать каталог</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="357"/>
        <source>Update library</source>
        <translation type="unfinished">Обновить библиотеку</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="358"/>
        <source>Update current library</source>
        <translation>Обновить текущую библиотеку</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="362"/>
        <source>Rename library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="363"/>
        <source>Rename current library</source>
        <translation>Переименовать текущую бибилиотеку</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="368"/>
        <source>Remove current library from your collection</source>
        <translation>Удалите текущую библиотеку из своей коллекции</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="371"/>
        <source>Open current comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="372"/>
        <source>Open current comic on YACReader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="376"/>
        <location filename="library_window.cpp" line="457"/>
        <source>Set as read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="377"/>
        <source>Set comic as read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="380"/>
        <location filename="library_window.cpp" line="461"/>
        <source>Set as unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="381"/>
        <source>Set comic as unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="392"/>
        <source>Show/Hide marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="393"/>
        <source>Show or hide readed marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="399"/>
        <source>Fullscreen mode on/off</source>
        <translation>Полноекранный режим включить/выключить</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="400"/>
        <source>Fullscreen mode on/off (F)</source>
        <translation>полноекранный режим включить/выключить(F)</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="407"/>
        <source>Help, About YACReader</source>
        <translation>Справка, о программе YACReader</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="415"/>
        <source>Select root node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="419"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="420"/>
        <source>Expand all nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="424"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="425"/>
        <source>Colapse all nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="430"/>
        <source>Show options dialog</source>
        <translation>Показать настройки диаога</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="437"/>
        <source>Show comics server options dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="445"/>
        <source>Open folder...</source>
        <translation>Открыть папку...</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="449"/>
        <source>Set as uncompleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="453"/>
        <source>Set as completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="465"/>
        <source>Open containing folder...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="469"/>
        <source>Reset comic rating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="473"/>
        <source>Select all comics</source>
        <translation>Выбрать все комиксы</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="477"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="481"/>
        <source>Asign current order to comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="485"/>
        <source>Update cover</source>
        <translation>Обновить обложки</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="489"/>
        <source>Delete selected comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="493"/>
        <source>Hide comic flow</source>
        <translation>Не показывать поток комиксов</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="499"/>
        <source>Download tags from Comic Vine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="705"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="715"/>
        <source>Comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="958"/>
        <source>Library not available</source>
        <oldsource>Library &apos;</oldsource>
        <translation type="unfinished"> Библиотека не доступна</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="958"/>
        <source>Library &apos;%1&apos; is no longer available. Do you want to remove it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="982"/>
        <source>Library &apos;%1&apos; has been created with an older version of YACReaderLibrary. It must be created again. Do you want to create the library now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="982"/>
        <source>Old library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1156"/>
        <source>YACReader not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1156"/>
        <source>YACReader not found, YACReader should be installed in the same folder as YACReaderLibrary.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1544"/>
        <source>Unable to delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1544"/>
        <source>There was an issue trying to delete the selected comics. Please, check for write permissions in the selected files or containing folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1733"/>
        <source>Error creating the library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1738"/>
        <source>Error updating the library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1743"/>
        <source>Error opening the library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1768"/>
        <source>Delete comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1768"/>
        <source>All the selected comics will be deleted from your disk. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1887"/>
        <source>Library name already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1887"/>
        <source>There is another library with the name &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="146"/>
        <location filename="library_window.cpp" line="689"/>
        <source>Library</source>
        <translation>Библиотека</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="367"/>
        <source>Remove library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="875"/>
        <source>Update needed</source>
        <translation>Необходимо обновление</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="875"/>
        <source>This library was created with a previous version of YACReaderLibrary. It needs to be updated. Update now?</source>
        <translation>Эта библиотека была создана с предыдущей версией YACReaderLibrary. Она должна быть обновлена. Обновить сейчас?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="881"/>
        <source>Update failed</source>
        <translation>Обновить неудалось</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="881"/>
        <source>The current library can&apos;t be udpated. Check for write write permissions on: </source>
        <translation>В настоящее время библиотека не может быть обновлена. Проверьте права на чтение/запись:</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="934"/>
        <source>Download new version</source>
        <translation>Загрузить новую версию</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="934"/>
        <source>This library was created with a newer version of YACReaderLibrary. Download the new version now?</source>
        <translation>Эта библиотека был создан при новой версией YACReaderLibrary. Скачать новую версию сейчас?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1255"/>
        <source>Library not found</source>
        <translation>Библиотека не найдена</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1255"/>
        <source>The selected folder doesn&apos;t contain any library.</source>
        <translation>Выбранная папка не содержит библиотеку.</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1328"/>
        <source>Are you sure?</source>
        <translation>Вы уверены?</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1328"/>
        <source> library?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1329"/>
        <source>Remove and delete metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1328"/>
        <source>Do you want remove </source>
        <translation>Вы хотите удалить</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1569"/>
        <source>Asign comics numbers</source>
        <translation>Назначение номеров комикса</translation>
    </message>
    <message>
        <location filename="library_window.cpp" line="1570"/>
        <source>Asign numbers starting in:</source>
        <translation>Назначьте номера, начинающиеся на:</translation>
    </message>
</context>
<context>
    <name>LocalComicListModel</name>
    <message>
        <location filename="comic_vine/model/local_comic_list_model.cpp" line="75"/>
        <source>file name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoLibrariesWidget</name>
    <message>
        <location filename="no_libraries_widget.cpp" line="26"/>
        <source>You don&apos;t have any librarires yet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="no_libraries_widget.cpp" line="28"/>
        <source>&lt;p&gt;You can create a library in any folder, YACReaderLibrary will import all comics and folders from this folder. If you have created any library in the past you can open them.&lt;/p&gt;&lt;p&gt;Don&apos;t forget that you can use YACReader as a stand alone application for reading the comics on your computer.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="no_libraries_widget.cpp" line="32"/>
        <source>create your first library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="no_libraries_widget.cpp" line="34"/>
        <source>add an existing one</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="options_dialog.cpp" line="47"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>PropertiesDialog</name>
    <message>
        <location filename="properties_dialog.cpp" line="74"/>
        <source>General info</source>
        <translation type="unfinished">Общия информация</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="75"/>
        <source>Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="76"/>
        <source>Publishing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="77"/>
        <source>Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="86"/>
        <source>Cover page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="147"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="163"/>
        <source>Issue number:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="165"/>
        <source>Volume:</source>
        <translation>Объём :</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="178"/>
        <source>Story arc:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="180"/>
        <source>Genere:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="182"/>
        <source>Size:</source>
        <translation>Размер:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="206"/>
        <source>Writer(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="209"/>
        <source>Penciller(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="217"/>
        <source>Inker(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="220"/>
        <source>Colorist(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="230"/>
        <source>Letterer(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="233"/>
        <source>Cover Artist(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="256"/>
        <source>Day:</source>
        <translation>День:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="260"/>
        <source>Month:</source>
        <translation>месяц:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="264"/>
        <source>Year:</source>
        <translation>Год:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="272"/>
        <source>Publisher:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="273"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="274"/>
        <source>Color/BW:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="275"/>
        <source>Age rating:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="288"/>
        <source>Synopsis:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="289"/>
        <source>Characters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="290"/>
        <source>Notes:</source>
        <translation>Примичяние:</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="388"/>
        <source>Comic Vine link: &lt;a style=&apos;color: #FFCB00; text-decoration:none; font-weight:bold;&apos; href=&quot;http://www.comicvine.com/comic/4000-%1/&quot;&gt; view &lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="416"/>
        <source>Not found</source>
        <translation type="unfinished">Не найдено</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="416"/>
        <source>Comic not found. You should update your library.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="491"/>
        <source>Edit selected comics information</source>
        <translation>Редактировать информацию выбранного комикса</translation>
    </message>
    <message>
        <location filename="properties_dialog.cpp" line="562"/>
        <source>Edit comic information</source>
        <translation type="unfinished">Реддактировать информацию </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>7z lib not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/exit_check.cpp" line="14"/>
        <source>unable to load 7z lib from ./utils</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="rename_library_dialog.cpp" line="17"/>
        <source>New Library Name : </source>
        <translation>Новое имя библиотеки:</translation>
    </message>
    <message>
        <location filename="rename_library_dialog.cpp" line="22"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="rename_library_dialog.cpp" line="26"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="rename_library_dialog.cpp" line="54"/>
        <source>Rename current library</source>
        <translation>Переименовать текущую бибилиотеку</translation>
    </message>
</context>
<context>
    <name>ScraperResultsPaginator</name>
    <message>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="32"/>
        <source>Number of volumes found : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="34"/>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="57"/>
        <source>page %1 of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/scraper_results_paginator.cpp" line="56"/>
        <source>Number of %1 found : %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchSingleComic</name>
    <message>
        <location filename="comic_vine/search_single_comic.cpp" line="14"/>
        <source>Please provide some additional information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/search_single_comic.cpp" line="19"/>
        <source>Series:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchVolume</name>
    <message>
        <location filename="comic_vine/search_volume.cpp" line="11"/>
        <source>Please provide some additional information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/search_volume.cpp" line="14"/>
        <source>Series:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectComic</name>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="17"/>
        <source>Please, select the right comic info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="40"/>
        <source>comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="99"/>
        <source>loading cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="100"/>
        <source>loading description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_comic.cpp" line="143"/>
        <source>description unavailable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectVolume</name>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="32"/>
        <source>Please, select the right series for your comic.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="63"/>
        <source>volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="128"/>
        <source>loading cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="129"/>
        <source>loading description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/select_volume.cpp" line="172"/>
        <source>description unavailable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeriesQuestion</name>
    <message>
        <location filename="comic_vine/series_question.cpp" line="13"/>
        <source>You are trying to get information for various comics at once, are they part of the same series?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/series_question.cpp" line="15"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/series_question.cpp" line="16"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerConfigDialog</name>
    <message>
        <location filename="server_config_dialog.cpp" line="61"/>
        <source>set port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="68"/>
        <source>EASY SERVER CONNECTION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="72"/>
        <source>SERVER ADDRESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="76"/>
        <source>just scan the code with your device!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="82"/>
        <source>YACReader is now available for iOS devices, the best comic reading experience now in your iPad, iPhone or iPod touch. &lt;a href=&apos;http://ios.yacreader.com&apos; style=&apos;color:rgb(193, 148, 65)&apos;&gt; Discover it! &lt;/a&gt;</source>
        <oldsource>YACReader is now available for iOS devices, the best comic reading experience now in your iPad, iPhone or iPod touch. &lt;a href=&apos;http://ios.yacreader.com&apos;&gt; Discover it! &lt;/a&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="128"/>
        <source>IP address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="129"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="142"/>
        <source>enable the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="server_config_dialog.cpp" line="294"/>
        <source>QR generator error!</source>
        <translation>Ошибка QR генератора!</translation>
    </message>
</context>
<context>
    <name>SortVolumeComics</name>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="18"/>
        <source>Please, sort the list of comics on the left until it matches the comics&apos; information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="21"/>
        <source>sort comics to match comic information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="62"/>
        <source>issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="91"/>
        <source>remove selected comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="92"/>
        <source>restore all removed comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/sort_volume_comics.cpp" line="93"/>
        <source>restore removed comics</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableModel</name>
    <message>
        <location filename="db/tablemodel.cpp" line="94"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="94"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="127"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="129"/>
        <source>File Name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="131"/>
        <source>Pages</source>
        <translation>Страницы</translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="133"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="135"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="137"/>
        <source>Current Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="db/tablemodel.cpp" line="139"/>
        <source>Rating</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleHeader</name>
    <message>
        <location filename="comic_vine/title_header.cpp" line="30"/>
        <source>SEARCH</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateLibraryDialog</name>
    <message>
        <location filename="create_library_dialog.cpp" line="166"/>
        <source>Updating....</source>
        <translation>Обновление...</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="172"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="create_library_dialog.cpp" line="191"/>
        <source>Update library</source>
        <translation>Обновить библиотеку</translation>
    </message>
</context>
<context>
    <name>VolumeComicsModel</name>
    <message>
        <location filename="comic_vine/model/volume_comics_model.cpp" line="122"/>
        <source>title</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumesModel</name>
    <message>
        <location filename="comic_vine/model/volumes_model.cpp" line="112"/>
        <source>year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/model/volumes_model.cpp" line="114"/>
        <source>issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comic_vine/model/volumes_model.cpp" line="116"/>
        <source>publisher</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderDeletingProgress</name>
    <message>
        <location filename="../custom_widgets/yacreader_deleting_progress.cpp" line="20"/>
        <source>Please wait, deleting in progress...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_deleting_progress.cpp" line="35"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YACReaderFieldEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="29"/>
        <source>Click to overwrite</source>
        <translation>Нажмите для перезаписи</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_edit.cpp" line="11"/>
        <source>Restore to default</source>
        <translation>Вернуть к первоначальным значениям</translation>
    </message>
</context>
<context>
    <name>YACReaderFieldPlainTextEdit</name>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="9"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="20"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="45"/>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="51"/>
        <source>Click to overwrite</source>
        <translation>Нажмите для перезаписи</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_field_plain_text_edit.cpp" line="10"/>
        <source>Restore to default</source>
        <translation>Вернуть к первоначальным значениям</translation>
    </message>
</context>
<context>
    <name>YACReaderFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="13"/>
        <source>How to show covers:</source>
        <translation>Как показать обложки:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="15"/>
        <source>CoverFlow look</source>
        <translation>Предосмотр обложки</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="16"/>
        <source>Stripe look</source>
        <translation>Вид полосами</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_flow_config_widget.cpp" line="17"/>
        <source>Overlapped Stripe look</source>
        <translation>Вид перекрывающимися полосами</translation>
    </message>
</context>
<context>
    <name>YACReaderGLFlowConfigWidget</name>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="19"/>
        <source>Presets:</source>
        <translation>Предустановки:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="21"/>
        <source>Classic look</source>
        <translation>Классический вид</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="24"/>
        <source>Stripe look</source>
        <translation>Вид полосами</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="27"/>
        <source>Overlapped Stripe look</source>
        <translation>Вид перекрывающимися полосами</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="30"/>
        <source>Modern look</source>
        <translation>Современный вид</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="33"/>
        <source>Roulette look</source>
        <translation>Вид рулеткой</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="77"/>
        <source>Show advanced settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="86"/>
        <source>Custom:</source>
        <translation>Пользовательский:</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="89"/>
        <source>View angle</source>
        <translation>Угол зрения</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="95"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="101"/>
        <source>Cover gap</source>
        <translation>Охватить разрыв</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="107"/>
        <source>Central gap</source>
        <translation>Сфокусировать разрыв</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="113"/>
        <source>Zoom</source>
        <translation>Масштабировать</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="119"/>
        <source>Y offset</source>
        <translation>Смещение по Y</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="125"/>
        <source>Z offset</source>
        <translation>Смещение по Z</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="131"/>
        <source>Cover Angle</source>
        <translation>Охватить угол</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="137"/>
        <source>Visibility</source>
        <translation>Прозрачность</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="143"/>
        <source>Light</source>
        <translation>Осветить</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="149"/>
        <source>Max angle</source>
        <translation>Максимальный угол</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="181"/>
        <source>Low Performance</source>
        <translation>Минимальная производительность</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="183"/>
        <source>High Performance</source>
        <translation>Максимальная производительность</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="194"/>
        <source>Use VSync (improve the image quality in fullscreen mode, worse performance)</source>
        <translation>Использовать VSync (повысить формат изображения в полноэкранном режиме , хуже производительность)</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_gl_flow_config_widget.cpp" line="202"/>
        <source>Performance:</source>
        <translation>Производительность:</translation>
    </message>
</context>
<context>
    <name>YACReaderOptionsDialog</name>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="21"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_options_dialog.cpp" line="30"/>
        <source>Use hardware acceleration (restart needed)</source>
        <translation>Использовать аппаратное ускорение (необходима перезагрузка)</translation>
    </message>
</context>
<context>
    <name>YACReaderSideBar</name>
    <message>
        <location filename="../custom_widgets/yacreader_sidebar.cpp" line="21"/>
        <source>LIBRARIES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_sidebar.cpp" line="23"/>
        <source>FOLDERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../custom_widgets/yacreader_sidebar.cpp" line="30"/>
        <source>Search folders and comics</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
