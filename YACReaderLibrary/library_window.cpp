#include "library_window.h"
#include "custom_widgets.h"
#include "treeitem.h"

#include <QHBoxLayout>
#include <QSplitter>
#include <QLabel>
#include <QDir>
#include <QDirModel>
#include <QHeaderView>
#include <QProcess>
#include <QtCore>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QFileIconProvider>
#include <QMatrix>
#include <QSettings>
#include <QGLFormat>
#include <QHeaderView>

#include <iterator>
#include <typeinfo>

#include "data_base_management.h"
#include "yacreader_global.h"
#include "onstart_flow_selection_dialog.h"
#include "no_libraries_widget.h"
#include "import_widget.h"

#include "yacreader_search_line_edit.h"
#include "comic_db.h"
#include "library_creator.h"
#include "package_manager.h"
#include "comic_flow_widget.h"
#include "create_library_dialog.h"
#include "rename_library_dialog.h"
#include "properties_dialog.h"
#include "export_library_dialog.h"
#include "import_library_dialog.h"
#include "export_comics_info_dialog.h"
#include "import_comics_info_dialog.h"
#include "add_library_dialog.h"
#include "options_dialog.h"
#include "help_about_dialog.h"
#include "server_config_dialog.h"
#include "tablemodel.h"
#include "yacreader_tool_bar_stretch.h"
#include "yacreader_table_view.h"

#include "yacreader_dark_menu.h"
#include "yacreader_titled_toolbar.h"
#include "yacreader_main_toolbar.h"

#include "yacreader_sidebar.h"

#include "comics_remover.h"
#include "yacreader_library_list_widget.h"
#include "yacreader_treeview.h"

#include "comic_vine_dialog.h"
//#include "yacreader_social_dialog.h"

#include "QsLog.h"

#ifdef Q_OS_WIN
    #include <shellapi.h>
#endif

#ifdef Q_OS_MAC
//#include <QtMacExtras>
#endif

LibraryWindow::LibraryWindow()
	:QMainWindow(),fullscreen(false),fetching(false),previousFilter(""),removeError(false)
{
	setupUI();
	loadLibraries();

	if(libraries.isEmpty())
	{
		showNoLibrariesWidget();
	}
	else
	{
		showRootWidget();
		selectedLibrary->setCurrentIndex(0);
	}
}

void LibraryWindow::setupUI()
{
	setWindowIcon(QIcon(":/images/iconLibrary.png"));

	setUnifiedTitleAndToolBarOnMac(true);

	libraryCreator = new LibraryCreator();
	packageManager = new PackageManager();

	settings = new QSettings(YACReader::getSettingsPath()+"/YACReaderLibrary.ini",QSettings::IniFormat); //TODO unificar la creaci�n del fichero de config con el servidor
	settings->beginGroup("libraryConfig");

	createActions();
	doModels();
	
	doLayout();
	createToolBars();
	doDialogs();
	createMenus();
	createConnections();

	setWindowTitle(tr("YACReader Library"));

	setMinimumSize(800,480);

	//restore
	if(settings->contains(MAIN_WINDOW_GEOMETRY))
		restoreGeometry(settings->value(MAIN_WINDOW_GEOMETRY).toByteArray());
	else
		//if(settings->value(USE_OPEN_GL).toBool() == false)
			showMaximized();
	if(settings->contains(COMICS_VIEW_HEADERS))
		comicView->horizontalHeader()->restoreState(settings->value(COMICS_VIEW_HEADERS).toByteArray());
	/*if(settings->contains(COMICS_VIEW_HEADERS_GEOMETRY))
		comicView->horizontalHeader()->restoreGeometry(settings->value(COMICS_VIEW_HEADERS_GEOMETRY).toByteArray());*/

	/*socialDialog = new YACReaderSocialDialog(this);
	socialDialog->setHidden(true);*/
}

void LibraryWindow::doLayout()
{
	//LAYOUT ELEMENTS------------------------------------------------------------
	//---------------------------------------------------------------------------
	sVertical = new QSplitter(Qt::Vertical);  //spliter derecha
	QSplitter * sHorizontal = new QSplitter(Qt::Horizontal);  //spliter principal
#ifdef Q_OS_MAC
	sHorizontal->setStyleSheet("QSplitter::handle{image:none;background-color:#B8B8B8;} QSplitter::handle:vertical {height:1px;}");
#else
	sHorizontal->setStyleSheet("QSplitter::handle:vertical {height:4px;}");
#endif

	//TOOLBARS-------------------------------------------------------------------
	//---------------------------------------------------------------------------
	editInfoToolBar = new QToolBar();
#ifdef Q_OS_MAC
	libraryToolBar = addToolBar(tr("Library"));
#else
	libraryToolBar = new YACReaderMainToolBar(this);
#endif

	//FLOW-----------------------------------------------------------------------
	//---------------------------------------------------------------------------
	if(QGLFormat::hasOpenGL() && !settings->contains(USE_OPEN_GL))
	{
		OnStartFlowSelectionDialog * flowSelDialog = new OnStartFlowSelectionDialog();

		flowSelDialog->exec();
		if(flowSelDialog->result() == QDialog::Accepted)
			settings->setValue(USE_OPEN_GL,2);
		else
			settings->setValue(USE_OPEN_GL,0);

		delete flowSelDialog;
	}

	if(QGLFormat::hasOpenGL() && (settings->value(USE_OPEN_GL).toBool() == true))
		comicFlow = new ComicFlowWidgetGL(0);
	else
		comicFlow = new ComicFlowWidgetSW(0);

	comicFlow->updateConfig(settings);
	comicFlow->setFocusPolicy(Qt::StrongFocus);
	comicFlow->setShowMarks(true);
	setFocusProxy(comicFlow);

	fullScreenToolTip = new QLabel(comicFlow);
	fullScreenToolTip->setText(tr("<font color='white'> press 'F' to close fullscreen mode </font>"));
	fullScreenToolTip->setPalette(QPalette(QColor(0,0,0)));
	fullScreenToolTip->setFont(QFont("courier new",15,234));
	fullScreenToolTip->setAutoFillBackground(true);
	fullScreenToolTip->hide();
	fullScreenToolTip->adjustSize();

	comicFlow->setFocus(Qt::OtherFocusReason);

	comicFlow->addAction(toggleFullScreenAction);
	comicFlow->addAction(openComicAction);

	//SIDEBAR-----------------------------------------------------------------------
	//---------------------------------------------------------------------------
	sideBar = new YACReaderSideBar;

	foldersView = sideBar->foldersView;
	selectedLibrary = sideBar->selectedLibrary;
	foldersFilter = sideBar->foldersFilter;

	YACReaderTitledToolBar * librariesTitle = sideBar->librariesTitle;

	YACReaderTitledToolBar * foldersTitle = sideBar->foldersTitle;

	librariesTitle->addAction(createLibraryAction);
	librariesTitle->addAction(openLibraryAction);
	librariesTitle->addSpacing(3);

	foldersTitle->addAction(setRootIndexAction);
	foldersTitle->addAction(expandAllNodesAction);
	foldersTitle->addAction(colapseAllNodesAction);

	//FINAL LAYOUT-------------------------------------------------------------
	sVertical->addWidget(comicFlow);
	comics = new QWidget;
	QVBoxLayout * comicsLayout = new QVBoxLayout;
	comicsLayout->setSpacing(0);
	comicsLayout->setContentsMargins(0,0,0,0);
	comicsLayout->addWidget(editInfoToolBar);

	editInfoToolBar->setStyleSheet("QToolBar {border: none;}");
	
	comicView = new YACReaderTableView;
	comicView->verticalHeader()->hide();
	comicsLayout->addWidget(comicView);
	comics->setLayout(comicsLayout);
	sVertical->addWidget(comics);
	sHorizontal->addWidget(sideBar);
#ifndef Q_OS_MAC
	QVBoxLayout * rightLayout = new QVBoxLayout;
	rightLayout->addWidget(libraryToolBar);
	rightLayout->addWidget(sVertical);

	rightLayout->setMargin(0);
	rightLayout->setSpacing(0);

	QWidget * rightWidget = new QWidget();
	rightWidget->setLayout(rightLayout);

	sHorizontal->addWidget(rightWidget);
#else
	sHorizontal->addWidget(sVertical);
#endif
	
	sHorizontal->setStretchFactor(0,0);
	sHorizontal->setStretchFactor(1,1);
	mainWidget = new QStackedWidget(this);
	mainWidget->addWidget(sHorizontal);
	setCentralWidget(mainWidget);
	//FINAL LAYOUT-------------------------------------------------------------


	//OTHER----------------------------------------------------------------------
	//---------------------------------------------------------------------------
	noLibrariesWidget = new NoLibrariesWidget();
	mainWidget->addWidget(noLibrariesWidget);

	importWidget = new ImportWidget();
	mainWidget->addWidget(importWidget);

	connect(noLibrariesWidget,SIGNAL(createNewLibrary()),this,SLOT(createLibrary()));
	connect(noLibrariesWidget,SIGNAL(addExistingLibrary()),this,SLOT(showAddLibrary()));

	comicFlow->addAction(toggleFullScreenAction);
	comicFlow->addAction(openComicAction);

	comicFlow->setContextMenuPolicy(Qt::ActionsContextMenu);

	//collapsible disabled in macosx (only temporaly)
#ifdef Q_OS_MAC
	sHorizontal->setCollapsible(0,false);
	sVertical->setCollapsible(1,false);
#endif
}

void LibraryWindow::doDialogs()
{
	createLibraryDialog = new CreateLibraryDialog(this);
	renameLibraryDialog = new RenameLibraryDialog(this);
	propertiesDialog = new PropertiesDialog(this);
	comicVineDialog = new ComicVineDialog(this);
	exportLibraryDialog = new ExportLibraryDialog(this);
	importLibraryDialog = new ImportLibraryDialog(this);
	exportComicsInfoDialog = new ExportComicsInfoDialog(this);
	importComicsInfoDialog = new ImportComicsInfoDialog(this);
	addLibraryDialog = new AddLibraryDialog(this);
	optionsDialog = new OptionsDialog(this);
	optionsDialog->restoreOptions(settings);

#ifdef SERVER_RELEASE
	serverConfigDialog = new ServerConfigDialog(this);
#endif

	had = new HelpAboutDialog(this); //TODO load data.
	QString sufix = QLocale::system().name();
	if(QFile(":/files/about_"+sufix+".html").exists())
		had->loadAboutInformation(":/files/about_"+sufix+".html");
	else
		had->loadAboutInformation(":/files/about.html");

	if(QFile(":/files/helpYACReaderLibrary_"+sufix+".html").exists())
		had->loadHelp(":/files/helpYACReaderLibrary_"+sufix+".html");
	else
		had->loadHelp(":/files/helpYACReaderLibrary.html");


}

void LibraryWindow::doModels()
{
	//folders
	dm = new TreeModel();
	//comics
	dmCV =  new TableModel();

	setFoldersFilter("");
}

void LibraryWindow::createActions()
{
	backAction = new QAction(this);
	QIcon icoBackButton;
	icoBackButton.addPixmap(QPixmap(":/images/main_toolbar/back.png"), QIcon::Normal);
	//icoBackButton.addPixmap(QPixmap(":/images/main_toolbar/back_disabled.png"), QIcon::Disabled);
	backAction->setIcon(icoBackButton);
	backAction->setDisabled(true);

	forwardAction = new QAction(this);
	QIcon icoFordwardButton;
	icoFordwardButton.addPixmap(QPixmap(":/images/main_toolbar/forward.png"), QIcon::Normal);
	//icoFordwardButton.addPixmap(QPixmap(":/images/main_toolbar/forward_disabled.png"), QIcon::Disabled);
	forwardAction->setIcon(icoFordwardButton);
	forwardAction->setDisabled(true);

	createLibraryAction = new QAction(this);
	createLibraryAction->setToolTip(tr("Create a new library"));
	createLibraryAction->setShortcut(Qt::Key_A);
	createLibraryAction->setIcon(QIcon(":/images/newLibraryIcon.png"));

	openLibraryAction = new QAction(this);
	openLibraryAction->setToolTip(tr("Open an existing library"));
	openLibraryAction->setShortcut(Qt::Key_O);
	openLibraryAction->setIcon(QIcon(":/images/openLibraryIcon.png"));

	exportComicsInfo = new QAction(tr("Export comics info"),this);
	exportComicsInfo->setToolTip(tr("Export comics info"));
	exportComicsInfo->setIcon(QIcon(":/images/exportComicsInfoIcon.png"));

	importComicsInfo = new QAction(tr("Import comics info"),this);
	importComicsInfo->setToolTip(tr("Import comics info"));
	importComicsInfo->setIcon(QIcon(":/images/importComicsInfoIcon.png"));

	exportLibraryAction = new QAction(tr("Pack covers"),this);
	exportLibraryAction->setToolTip(tr("Pack the covers of the selected library"));
	exportLibraryAction->setIcon(QIcon(":/images/exportLibraryIcon.png"));

	importLibraryAction = new QAction(tr("Unpack covers"),this);
	importLibraryAction->setToolTip(tr("Unpack a catalog"));
	importLibraryAction->setIcon(QIcon(":/images/importLibraryIcon.png"));

	updateLibraryAction = new QAction(tr("Update library"),this);
	updateLibraryAction->setToolTip(tr("Update current library"));
	updateLibraryAction->setShortcut(Qt::Key_U);
	updateLibraryAction->setIcon(QIcon(":/images/updateLibraryIcon.png"));

	renameLibraryAction = new QAction(tr("Rename library"),this);
	renameLibraryAction->setToolTip(tr("Rename current library"));
	renameLibraryAction->setShortcut(Qt::Key_R);
	renameLibraryAction->setIcon(QIcon(":/images/editIcon.png"));

	removeLibraryAction = new QAction(tr("Remove library"),this);
	removeLibraryAction->setToolTip(tr("Remove current library from your collection"));
	removeLibraryAction->setIcon(QIcon(":/images/removeLibraryIcon.png"));

	openComicAction = new QAction(tr("Open current comic"),this);
	openComicAction->setToolTip(tr("Open current comic on YACReader"));
	openComicAction->setShortcut(Qt::Key_Return);
	openComicAction->setIcon(QIcon(":/images/openInYACReader.png"));

	setAsReadAction = new QAction(tr("Set as read"),this);
	setAsReadAction->setToolTip(tr("Set comic as read"));
	setAsReadAction->setIcon(QIcon(":/images/setReadButton.png"));

	setAsNonReadAction = new QAction(tr("Set as unread"),this);
	setAsNonReadAction->setToolTip(tr("Set comic as unread"));
	setAsNonReadAction->setIcon(QIcon(":/images/setUnread.png"));

	/*setAllAsReadAction = new QAction(tr("Set all as read"),this);
	setAllAsReadAction->setToolTip(tr("Set all comics as read"));
	setAllAsReadAction->setIcon(QIcon(":/images/setAllRead.png"));

	setAllAsNonReadAction = new QAction(tr("Set all as unread"),this);
	setAllAsNonReadAction->setToolTip(tr("Set all comics as unread"));
	setAllAsNonReadAction->setIcon(QIcon(":/images/setAllUnread.png"));*/

	showHideMarksAction = new QAction(tr("Show/Hide marks"),this);
	showHideMarksAction->setToolTip(tr("Show or hide readed marks"));
	showHideMarksAction->setShortcut(Qt::Key_M);
	showHideMarksAction->setCheckable(true);
	showHideMarksAction->setIcon(QIcon(":/images/showMarks.png"));
	showHideMarksAction->setChecked(true);

	toggleFullScreenAction = new QAction(tr("Fullscreen mode on/off"),this);
	toggleFullScreenAction->setToolTip(tr("Fullscreen mode on/off (F)"));
	toggleFullScreenAction->setShortcut(Qt::Key_F);
	QIcon icoFullscreenButton;
	icoFullscreenButton.addPixmap(QPixmap(":/images/main_toolbar/fullscreen.png"), QIcon::Normal);
	toggleFullScreenAction->setIcon(icoFullscreenButton);

	helpAboutAction = new QAction(this);
	helpAboutAction->setToolTip(tr("Help, About YACReader"));
	helpAboutAction->setShortcut(Qt::Key_F1);
	QIcon icoHelpButton;
	icoHelpButton.addPixmap(QPixmap(":/images/main_toolbar/help.png"), QIcon::Normal);
	helpAboutAction->setIcon(icoHelpButton);

	setRootIndexAction = new QAction(this);
	setRootIndexAction->setShortcut(Qt::Key_0);
	setRootIndexAction->setToolTip(tr("Select root node"));
	setRootIndexAction->setIcon(QIcon(":/images/setRoot.png"));

	expandAllNodesAction = new QAction(this);
	expandAllNodesAction->setShortcut(tr("+"));
	expandAllNodesAction->setToolTip(tr("Expand all nodes"));
	expandAllNodesAction->setIcon(QIcon(":/images/expand.png"));

	colapseAllNodesAction = new QAction(this);
	colapseAllNodesAction->setShortcut(tr("-"));
	colapseAllNodesAction->setToolTip(tr("Colapse all nodes"));
	colapseAllNodesAction->setIcon(QIcon(":/images/colapse.png"));

	optionsAction = new QAction(this);
	optionsAction->setShortcut(Qt::Key_C);
	optionsAction->setToolTip(tr("Show options dialog"));
	QIcon icoSettingsButton;
	icoSettingsButton.addPixmap(QPixmap(":/images/main_toolbar/settings.png"), QIcon::Normal);
	optionsAction->setIcon(icoSettingsButton);

	serverConfigAction = new QAction(this);
	serverConfigAction->setShortcut(Qt::Key_S);
	serverConfigAction->setToolTip(tr("Show comics server options dialog"));
	QIcon icoServerButton;
	icoServerButton.addPixmap(QPixmap(":/images/main_toolbar/server.png"), QIcon::Normal);
	serverConfigAction->setIcon(icoServerButton);

	//socialAction = new QAction(this);

	openContainingFolderAction = new QAction(this);
	openContainingFolderAction->setText(tr("Open folder..."));
	openContainingFolderAction->setIcon(QIcon(":/images/open.png"));

    setFolderAsNotCompletedAction = new QAction(this);
    setFolderAsNotCompletedAction->setText(tr("Set as uncompleted"));
    setFolderAsNotCompletedAction->setVisible(false);

    setFolderAsCompletedAction = new QAction(this);
    setFolderAsCompletedAction->setText(tr("Set as completed"));
    setFolderAsCompletedAction->setVisible(false);

    setFolderAsFinishedAction = new QAction(this);
    setFolderAsFinishedAction->setText(tr("Set as read"));
    setFolderAsFinishedAction->setVisible(false);

    setFolderAsNotFinishedAction = new QAction(this);
    setFolderAsNotFinishedAction->setText(tr("Set as unread"));
    setFolderAsNotFinishedAction->setVisible(false);

	openContainingFolderComicAction = new QAction(this);
	openContainingFolderComicAction->setText(tr("Open containing folder..."));
	openContainingFolderComicAction->setIcon(QIcon(":/images/open.png"));

    resetComicRatingAction = new QAction(this);
    resetComicRatingAction->setText(tr("Reset comic rating"));

	//Edit comics actions------------------------------------------------------
	selectAllComicsAction = new QAction(this);
	selectAllComicsAction->setText(tr("Select all comics"));
	selectAllComicsAction->setIcon(QIcon(":/images/selectAll.png"));

	editSelectedComicsAction = new QAction(this);
	editSelectedComicsAction->setText(tr("Edit"));
	editSelectedComicsAction->setIcon(QIcon(":/images/editComic.png"));

	asignOrderActions = new QAction(this);
	asignOrderActions->setText(tr("Asign current order to comics"));
	asignOrderActions->setIcon(QIcon(":/images/asignNumber.png"));

	forceConverExtractedAction = new QAction(this);
	forceConverExtractedAction->setText(tr("Update cover"));
	forceConverExtractedAction->setIcon(QIcon(":/images/importCover.png"));

	deleteComicsAction = new QAction(this);
	deleteComicsAction->setText(tr("Delete selected comics"));
	deleteComicsAction->setIcon(QIcon(":/images/trash.png"));

	hideComicViewAction = new QAction(this);
	hideComicViewAction->setText(tr("Hide comic flow"));
	hideComicViewAction->setIcon(QIcon(":/images/hideComicFlow.png"));
	hideComicViewAction->setCheckable(true);
	hideComicViewAction->setChecked(false);

	getInfoAction = new QAction(this);
	getInfoAction->setText(tr("Download tags from Comic Vine"));
	getInfoAction->setIcon(QIcon(":/images/getInfo.png"));
	//-------------------------------------------------------------------------

	//disable actions
	disableAllActions();
}
void LibraryWindow::disableComicsActions(bool disabled)
{
	//if there aren't comics, no fullscreen option will be available
	toggleFullScreenAction->setDisabled(disabled);
	//edit toolbar
	openComicAction->setDisabled(disabled);
	editSelectedComicsAction->setDisabled(disabled);
	selectAllComicsAction->setDisabled(disabled);
	asignOrderActions->setDisabled(disabled);
	setAsReadAction->setDisabled(disabled);
	setAsNonReadAction->setDisabled(disabled);
	//setAllAsReadAction->setDisabled(disabled);
	//setAllAsNonReadAction->setDisabled(disabled);
	showHideMarksAction->setDisabled(disabled);
	deleteComicsAction->setDisabled(disabled);
	//context menu
	openContainingFolderComicAction->setDisabled(disabled);
    resetComicRatingAction->setDisabled(disabled);

	getInfoAction->setDisabled(disabled);


}
void LibraryWindow::disableLibrariesActions(bool disabled)
{
	updateLibraryAction->setDisabled(disabled);
	renameLibraryAction->setDisabled(disabled);
	removeLibraryAction->setDisabled(disabled);
	exportComicsInfo->setDisabled(disabled);
	importComicsInfo->setDisabled(disabled);
	exportLibraryAction->setDisabled(disabled);
	//importLibraryAction->setDisabled(disabled);
}

void LibraryWindow::disableNoUpdatedLibrariesActions(bool disabled)
{
	updateLibraryAction->setDisabled(disabled);
	exportComicsInfo->setDisabled(disabled);
	importComicsInfo->setDisabled(disabled);
	exportLibraryAction->setDisabled(disabled);
}

void LibraryWindow::disableFoldersActions(bool disabled)
{
	setRootIndexAction->setDisabled(disabled);
	expandAllNodesAction->setDisabled(disabled);
	colapseAllNodesAction->setDisabled(disabled);

	openContainingFolderAction->setDisabled(disabled);

    if(disabled == false)
    {
        setFolderAsNotCompletedAction->setVisible(false);
        setFolderAsCompletedAction->setVisible(false);
        setFolderAsFinishedAction->setVisible(false);
        setFolderAsNotFinishedAction->setVisible(false);
    }
}

void LibraryWindow::disableAllActions()
{
	disableComicsActions(true);
	disableLibrariesActions(true);
	disableFoldersActions(true);
}

void LibraryWindow::createToolBars()
{

#ifdef Q_OS_MAC
	libraryToolBar->setIconSize(QSize(16,16)); //TODO make icon size dynamic

	libraryToolBar->addAction(backAction);
	libraryToolBar->addAction(forwardAction);

	{QWidget * w = new QWidget();
	w->setFixedWidth(10);
	libraryToolBar->addWidget(w);}

#ifdef SERVER_RELEASE
	libraryToolBar->addAction(serverConfigAction);
#endif
	libraryToolBar->addAction(optionsAction);
	libraryToolBar->addAction(helpAboutAction);

	{ QWidget * w2 = new QWidget();
	 w2->setFixedWidth(10);
	 libraryToolBar->addWidget(w2);}

	libraryToolBar->addAction(toggleFullScreenAction);

	libraryToolBar->addWidget(new QToolBarStretch());
	libraryToolBar->addWidget(foldersFilter);

	libraryToolBar->setMovable(false);

	
#else
	libraryToolBar->backButton->setDefaultAction(backAction);
	libraryToolBar->forwardButton->setDefaultAction(forwardAction);
	libraryToolBar->settingsButton->setDefaultAction(optionsAction);
	libraryToolBar->serverButton->setDefaultAction(serverConfigAction);
	libraryToolBar->helpButton->setDefaultAction(helpAboutAction);
	libraryToolBar->fullscreenButton->setDefaultAction(toggleFullScreenAction);
#endif

	editInfoToolBar->setIconSize(QSize(18,18));
	editInfoToolBar->addAction(openComicAction);
	editInfoToolBar->addSeparator();
	editInfoToolBar->addAction(editSelectedComicsAction);
	editInfoToolBar->addAction(getInfoAction);
	editInfoToolBar->addAction(asignOrderActions);
	
	editInfoToolBar->addSeparator();
	
	editInfoToolBar->addAction(selectAllComicsAction);

	editInfoToolBar->addSeparator();

	editInfoToolBar->addAction(setAsReadAction);
	//editInfoToolBar->addAction(setAllAsReadAction);
	editInfoToolBar->addAction(setAsNonReadAction);
	//editInfoToolBar->addAction(setAllAsNonReadAction);

	editInfoToolBar->addAction(showHideMarksAction);

	editInfoToolBar->addSeparator();
	
	editInfoToolBar->addAction(deleteComicsAction);

	editInfoToolBar->addWidget(new QToolBarStretch());
	editInfoToolBar->addAction(hideComicViewAction);

}

void LibraryWindow::createMenus()
{
	comicView->addAction(openContainingFolderComicAction);
    YACReader::addSperator(comicView);

	comicView->addAction(resetComicRatingAction);
    YACReader::addSperator(comicView);

	comicView->addAction(editSelectedComicsAction);
	comicView->addAction(getInfoAction);
    comicView->addAction(asignOrderActions);
    YACReader::addSperator(comicView);

	comicView->addAction(setAsReadAction);
	comicView->addAction(setAsNonReadAction);
    YACReader::addSperator(comicView);

	comicView->addAction(deleteComicsAction);

	foldersView->addAction(openContainingFolderAction);
    YACReader::addSperator(foldersView);

    foldersView->addAction(setFolderAsNotCompletedAction);
    foldersView->addAction(setFolderAsCompletedAction);
    YACReader::addSperator(foldersView);

    foldersView->addAction(setFolderAsFinishedAction);
    foldersView->addAction(setFolderAsNotFinishedAction);

	selectedLibrary->addAction(updateLibraryAction); 
	selectedLibrary->addAction(renameLibraryAction);
	selectedLibrary->addAction(removeLibraryAction);
    YACReader::addSperator(selectedLibrary);

	selectedLibrary->addAction(exportComicsInfo);
	selectedLibrary->addAction(importComicsInfo);
    YACReader::addSperator(selectedLibrary);

	selectedLibrary->addAction(exportLibraryAction);
	selectedLibrary->addAction(importLibraryAction);
	
//MacOSX app menus
#ifdef Q_OS_MACX
    QMenuBar * menu = this->menuBar();
    //about / preferences
    //TODO

    //library
    QMenu * libraryMenu = new QMenu(tr("Library"));

    libraryMenu->addAction(updateLibraryAction);
    libraryMenu->addAction(renameLibraryAction);
    libraryMenu->addAction(removeLibraryAction);
    libraryMenu->addSeparator();

    libraryMenu->addAction(exportComicsInfo);
    libraryMenu->addAction(importComicsInfo);

    libraryMenu->addSeparator();

    libraryMenu->addAction(exportLibraryAction);
    libraryMenu->addAction(importLibraryAction);

    //folder
    QMenu * folderMenu = new QMenu(tr("Folder"));
    folderMenu->addAction(openContainingFolderAction);
    folderMenu->addSeparator();
    folderMenu->addAction(setFolderAsNotCompletedAction);
    folderMenu->addAction(setFolderAsCompletedAction);
    folderMenu->addSeparator();
    folderMenu->addAction(setFolderAsFinishedAction);
    folderMenu->addAction(setFolderAsNotFinishedAction);

    //comic
    QMenu * comicMenu = new QMenu(tr("Comic"));
    comicMenu->addAction(openContainingFolderComicAction);
    comicMenu->addSeparator();
    comicMenu->addAction(resetComicRatingAction);

    menu->addMenu(libraryMenu);
    menu->addMenu(folderMenu);
    menu->addMenu(comicMenu);
#endif
}

void LibraryWindow::createConnections()
{
	//history navigation
	connect(backAction,SIGNAL(triggered()),this,SLOT(backward()));
	connect(forwardAction,SIGNAL(triggered()),this,SLOT(forward()));

	//libraryCreator connections
	connect(createLibraryDialog,SIGNAL(createLibrary(QString,QString,QString)),this,SLOT(create(QString,QString,QString)));
	connect(createLibraryDialog,SIGNAL(libraryExists(QString)),this,SLOT(libraryAlreadyExists(QString)));
	connect(importComicsInfoDialog,SIGNAL(finished(int)),this,SLOT(reloadCurrentLibrary()));

	//connect(libraryCreator,SIGNAL(coverExtracted(QString)),createLibraryDialog,SLOT(showCurrentFile(QString)));
	//connect(libraryCreator,SIGNAL(coverExtracted(QString)),updateLibraryDialog,SLOT(showCurrentFile(QString)));
	connect(libraryCreator,SIGNAL(finished()),this,SLOT(showRootWidget()));
	connect(libraryCreator,SIGNAL(updated()),this,SLOT(reloadCurrentLibrary()));
	connect(libraryCreator,SIGNAL(created()),this,SLOT(openLastCreated()));
	connect(libraryCreator,SIGNAL(comicAdded(QString,QString)),importWidget,SLOT(newComic(QString,QString)));
	//libraryCreator errors
	connect(libraryCreator,SIGNAL(failedCreatingDB(QString)),this,SLOT(manageCreatingError(QString)));
	connect(libraryCreator,SIGNAL(failedUpdatingDB(QString)),this,SLOT(manageUpdatingError(QString))); //TODO: implement failedUpdatingDB
	
	//new import widget
	connect(importWidget,SIGNAL(stop()),this,SLOT(stopLibraryCreator()));

	//packageManager connections
	connect(exportLibraryDialog,SIGNAL(exportPath(QString)),this,SLOT(exportLibrary(QString)));
	connect(exportLibraryDialog,SIGNAL(rejected()),packageManager,SLOT(cancel()));
	connect(packageManager,SIGNAL(exported()),exportLibraryDialog,SLOT(close()));
	connect(importLibraryDialog,SIGNAL(unpackCLC(QString,QString,QString)),this,SLOT(importLibrary(QString,QString,QString)));
	connect(importLibraryDialog,SIGNAL(rejected()),packageManager,SLOT(cancel()));
	connect(importLibraryDialog,SIGNAL(rejected()),this,SLOT(deleteCurrentLibrary()));
	connect(importLibraryDialog,SIGNAL(libraryExists(QString)),this,SLOT(libraryAlreadyExists(QString)));
	connect(packageManager,SIGNAL(imported()),importLibraryDialog,SLOT(hide()));
	connect(packageManager,SIGNAL(imported()),this,SLOT(openLastCreated()));


	//create and update dialogs
	connect(createLibraryDialog,SIGNAL(cancelCreate()),this,SLOT(cancelCreating()));

	//open existing library from dialog.
	connect(addLibraryDialog,SIGNAL(addLibrary(QString,QString)),this,SLOT(openLibrary(QString,QString)));

	//load library when selected library changes
	connect(selectedLibrary,SIGNAL(currentIndexChanged(QString)),this,SLOT(loadLibrary(QString)));

	//rename library dialog
	connect(renameLibraryDialog,SIGNAL(renameLibrary(QString)),this,SLOT(rename(QString)));

	//navigations between view modes (tree,list and flow)
    connect(foldersView, SIGNAL(pressed(QModelIndex)), this, SLOT(updateFoldersViewConextMenu(QModelIndex)));
	connect(foldersView, SIGNAL(clicked(QModelIndex)), this, SLOT(loadCovers(QModelIndex)));
	connect(foldersView, SIGNAL(clicked(QModelIndex)), this, SLOT(updateHistory(QModelIndex)));

	connect(comicView, SIGNAL(clicked(QModelIndex)), this, SLOT(centerComicFlow(QModelIndex)));
	connect(comicFlow, SIGNAL(centerIndexChanged(int)), this, SLOT(updateComicView(int)));
	connect(comicView, SIGNAL(comicRated(int,QModelIndex)), dmCV, SLOT(updateRating(int,QModelIndex)));

	//actions
	connect(createLibraryAction,SIGNAL(triggered()),this,SLOT(createLibrary()));
	connect(exportLibraryAction,SIGNAL(triggered()),exportLibraryDialog,SLOT(show()));
	connect(importLibraryAction,SIGNAL(triggered()),this,SLOT(importLibraryPackage()));

	connect(openLibraryAction,SIGNAL(triggered()),this,SLOT(showAddLibrary()));
	connect(setAsReadAction,SIGNAL(triggered()),this,SLOT(setCurrentComicReaded()));
	connect(setAsNonReadAction,SIGNAL(triggered()),this,SLOT(setCurrentComicUnreaded()));
	//connect(setAllAsReadAction,SIGNAL(triggered()),this,SLOT(setComicsReaded()));
	//connect(setAllAsNonReadAction,SIGNAL(triggered()),this,SLOT(setComicsUnreaded()));

	connect(showHideMarksAction,SIGNAL(toggled(bool)),comicFlow,SLOT(setShowMarks(bool)));
	
	//comicsInfoManagement
	connect(exportComicsInfo,SIGNAL(triggered()),this,SLOT(showExportComicsInfo()));
	connect(importComicsInfo,SIGNAL(triggered()),this,SLOT(showImportComicsInfo()));

	//properties & config
	connect(propertiesDialog,SIGNAL(accepted()),this,SLOT(reloadCovers()));

	//comic vine
	connect(comicVineDialog,SIGNAL(accepted()),this,SLOT(reloadCovers()));

	connect(updateLibraryAction,SIGNAL(triggered()),this,SLOT(updateLibrary()));
	connect(renameLibraryAction,SIGNAL(triggered()),this,SLOT(renameLibrary()));
	//connect(deleteLibraryAction,SIGNAL(triggered()),this,SLOT(deleteLibrary()));
	connect(removeLibraryAction,SIGNAL(triggered()),this,SLOT(removeLibrary()));
	connect(openComicAction,SIGNAL(triggered()),this,SLOT(openComic()));
	connect(helpAboutAction,SIGNAL(triggered()),had,SLOT(show()));
	connect(setRootIndexAction,SIGNAL(triggered()),this,SLOT(setRootIndex()));
	connect(expandAllNodesAction,SIGNAL(triggered()),foldersView,SLOT(expandAll()));
	connect(colapseAllNodesAction,SIGNAL(triggered()),foldersView,SLOT(collapseAll()));
	connect(toggleFullScreenAction,SIGNAL(triggered()),this,SLOT(toggleFullScreen()));
	connect(optionsAction, SIGNAL(triggered()),optionsDialog,SLOT(show()));
#ifdef SERVER_RELEASE
	connect(serverConfigAction, SIGNAL(triggered()), serverConfigDialog, SLOT(show()));
#endif
	connect(optionsDialog, SIGNAL(optionsChanged()),this,SLOT(reloadOptions()));
	//ComicFlow
	connect(comicFlow,SIGNAL(selected(unsigned int)),this,SLOT(openComic()));
	connect(comicView,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(openComic()));
	//Folders filter
	//connect(clearFoldersFilter,SIGNAL(clicked()),foldersFilter,SLOT(clear()));
	connect(foldersFilter,SIGNAL(textChanged(QString)),this,SLOT(setFoldersFilter(QString)));
	//connect(includeComicsCheckBox,SIGNAL(stateChanged(int)),this,SLOT(searchInFiles(int)));

	//ContextMenus
	connect(openContainingFolderComicAction,SIGNAL(triggered()),this,SLOT(openContainingFolderComic()));
    connect(setFolderAsNotCompletedAction,SIGNAL(triggered()),this,SLOT(setFolderAsNotCompleted()));
    connect(setFolderAsCompletedAction,SIGNAL(triggered()),this,SLOT(setFolderAsCompleted()));
    connect(setFolderAsFinishedAction,SIGNAL(triggered()),this,SLOT(setFolderAsFinished()));
    connect(setFolderAsNotFinishedAction,SIGNAL(triggered()),this,SLOT(setFolderAsNotFinished()));
    connect(openContainingFolderAction,SIGNAL(triggered()),this,SLOT(openContainingFolder()));
    connect(resetComicRatingAction,SIGNAL(triggered()),this,SLOT(resetComicRating()));

	//connect(dm,SIGNAL(directoryLoaded(QString)),foldersView,SLOT(expandAll()));
	//connect(dm,SIGNAL(directoryLoaded(QString)),this,SLOT(updateFoldersView(QString)));
	//Comicts edition
	connect(selectAllComicsAction,SIGNAL(triggered()),comicView,SLOT(selectAll()));
	connect(editSelectedComicsAction,SIGNAL(triggered()),this,SLOT(showProperties()));
	connect(asignOrderActions,SIGNAL(triggered()),this,SLOT(asignNumbers()));

	connect(deleteComicsAction,SIGNAL(triggered()),this,SLOT(deleteComics()));

	connect(hideComicViewAction, SIGNAL(toggled(bool)),this, SLOT(hideComicFlow(bool)));

	connect(getInfoAction,SIGNAL(triggered()),this,SLOT(showComicVineScraper()));

	//connect(socialAction,SIGNAL(triggered()),this,SLOT(showSocial()));

}

void LibraryWindow::loadLibrary(const QString & name)
{
	if(!libraries.isEmpty())  //si hay bibliotecas...
	{	
		currentFolderNavigation=0;
		backAction->setDisabled(true);
		forwardAction->setDisabled(true);
		history.clear();
		history.append(QModelIndex());

		showRootWidget();
		QString path=libraries.getPath(name)+"/.yacreaderlibrary";
		QDir d; //TODO change this by static methods (utils class?? with delTree for example)
		QString dbVersion;
		if(d.exists(path) && d.exists(path+"/library.ydb") && (dbVersion = DataBaseManagement::checkValidDB(path+"/library.ydb")) != "") //si existe en disco la biblioteca seleccionada, y es v�lida..
		{
			int comparation = DataBaseManagement::compareVersions(dbVersion,VERSION);
			bool updated = false;
			if(comparation < 0)
				{
					int ret = QMessageBox::question(this,tr("Update needed"),tr("This library was created with a previous version of YACReaderLibrary. It needs to be updated. Update now?"),QMessageBox::Yes,QMessageBox::No);
					if(ret == QMessageBox::Yes)
					{
						//TODO update to new version
						updated = DataBaseManagement::updateToCurrentVersion(path+"/library.ydb");
						if(!updated)
							QMessageBox::critical(this,tr("Update failed"), tr("The current library can't be udpated. Check for write write permissions on: ") + path+"/library.ydb");
					}
					else
					{
						comicView->setModel(NULL);
						foldersView->setModel(NULL);
						comicFlow->clear();
						disableAllActions();//TODO comprobar que se deben deshabilitar
						//ser� possible renombrar y borrar estas bibliotecas
						renameLibraryAction->setEnabled(true);
						removeLibraryAction->setEnabled(true);
					}
				}

			if(comparation == 0 || updated) //en caso de que la versi�n se igual que la actual
			{
				index = 0;

				dm->setupModelData(path);
				foldersView->setModel(dm);

				if(dm->rowCount(QModelIndex())>0)
					disableFoldersActions(false);
				else
					disableFoldersActions(true);

				d.setCurrent(libraries.getPath(name));
				d.setFilter(QDir::AllDirs | QDir::Files | QDir::Hidden | QDir::NoSymLinks | QDir::NoDotAndDotDot);
				if(d.count()<=1) //librer�a de s�lo lectura
				{
					//QMessageBox::critical(NULL,QString::number(d.count()),QString::number(d.count()));
					disableLibrariesActions(false);
					updateLibraryAction->setDisabled(true);
					openContainingFolderAction->setDisabled(true);
					disableComicsActions(true);
					toggleFullScreenAction->setEnabled(true);

					importedCovers = true;
				}
				else //librer�a normal abierta
				{
					disableLibrariesActions(false);
					importedCovers = false;
				}

				setRootIndex();
				//TODO encontrar el bug que provoca que no se carguen adecuadamente las car�tulas en root.
				setRootIndex();

				foldersFilter->clear();
			}
			else if(comparation > 0)
			{
					int ret = QMessageBox::question(this,tr("Download new version"),tr("This library was created with a newer version of YACReaderLibrary. Download the new version now?"),QMessageBox::Yes,QMessageBox::No);
					if(ret == QMessageBox::Yes)
						QDesktopServices::openUrl(QUrl("http://www.yacreader.com"));

					comicView->setModel(NULL);
					foldersView->setModel(NULL);
					comicFlow->clear();
					disableAllActions();//TODO comprobar que se deben deshabilitar
					//ser� possible renombrar y borrar estas bibliotecas
					renameLibraryAction->setEnabled(true);
					removeLibraryAction->setEnabled(true);
			}
		}
		else
		{
			comicView->setModel(NULL);
			foldersView->setModel(NULL);
			comicFlow->clear();
			disableAllActions();//TODO comprobar que se deben deshabilitar

			//si la librer�a no existe en disco, se ofrece al usuario la posibiliad de eliminarla
			if(!d.exists(path))
			{
				QString currentLibrary = selectedLibrary->currentText();
				if(QMessageBox::question(this,tr("Library not available"),tr("Library '%1' is no longer available. Do you want to remove it?").arg(currentLibrary),QMessageBox::Yes,QMessageBox::No)==QMessageBox::Yes)
				{
					deleteCurrentLibrary();
				}
				//ser� possible renombrar y borrar estas bibliotecas
				renameLibraryAction->setEnabled(true);
				removeLibraryAction->setEnabled(true);

			}
			else//si existe el path, puede ser que la librer�a sea alguna versi�n pre-5.0 � que est� corrupta o que no haya drivers sql
			{
				
				if(d.exists(path+"/library.ydb"))
				{
					QSqlDatabase db = DataBaseManagement::loadDatabase(path);
					manageOpeningLibraryError(db.lastError().databaseText() + "-" + db.lastError().driverText());
					//ser� possible renombrar y borrar estas bibliotecas
					renameLibraryAction->setEnabled(true);
					removeLibraryAction->setEnabled(true);
				}
				else
				{
					QString currentLibrary = selectedLibrary->currentText();
					QString path = libraries.getPath(selectedLibrary->currentText());
					if(QMessageBox::question(this,tr("Old library"),tr("Library '%1' has been created with an older version of YACReaderLibrary. It must be created again. Do you want to create the library now?").arg(currentLibrary),QMessageBox::Yes,QMessageBox::No)==QMessageBox::Yes)
					{
						QDir d(path+"/.yacreaderlibrary");
						delTree(d);
						d.rmdir(path+"/.yacreaderlibrary");
						createLibraryDialog->setDataAndStart(currentLibrary,path);
						//create(path,path+"/.yacreaderlibrary",currentLibrary);
					}
					//ser� possible renombrar y borrar estas bibliotecas
					renameLibraryAction->setEnabled(true);
					removeLibraryAction->setEnabled(true);
				}
			}
		}
	}
	else //en caso de que no exista ninguna biblioteca se desactivan los botones pertinentes
	{
		disableAllActions();
		showNoLibrariesWidget();
	}
}

void LibraryWindow::loadCovers(const QModelIndex & mi)
{
	unsigned long long int folderId = 1;
	if(mi.isValid())
	{
		TreeItem *item = static_cast<TreeItem*>(mi.internalPointer());
		folderId = item->id;
#ifndef Q_OS_MAC
		libraryToolBar->setCurrentFolderName(item->data(0).toString());
#endif
	}
#ifndef Q_OS_MAC
	else libraryToolBar->setCurrentFolderName(selectedLibrary->currentText());
#endif



	//cambiado de orden, ya que al llamar a foldersFilter->clear() se invalidan los model index
	if(foldersFilter->text()!="")
	{
		//setFoldersFilter("");
		if(mi.isValid())
		{
			index = static_cast<TreeItem *>(mi.internalPointer())->originalItem;
			column = mi.column();
			foldersFilter->clear();
		}
	}
	else
	{
		index = static_cast<TreeItem *>(mi.internalPointer());
		column = mi.column();
	}

	//comicView->setModel(NULL);
	dmCV->setupModelData(folderId,dm->getDatabase());
	
	comicView->setModel(dmCV);
	comicView->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
#if QT_VERSION >= 0x050000
	comicView->horizontalHeader()->setSectionsMovable(true);
#else
	comicView->horizontalHeader()->setMovable(true);
#endif
	//TODO parametrizar la configuraci�n de las columnas
	for(int i = 0;i<comicView->horizontalHeader()->count();i++)
		comicView->horizontalHeader()->hideSection(i);

	comicView->horizontalHeader()->showSection(0);
	comicView->horizontalHeader()->showSection(1);
	comicView->horizontalHeader()->showSection(2);
	comicView->horizontalHeader()->showSection(3);
	comicView->horizontalHeader()->showSection(7);
	comicView->horizontalHeader()->showSection(8);
	comicView->horizontalHeader()->showSection(10);
	comicView->horizontalHeader()->showSection(11);


	//debido a un bug, qt4 no es capaz de ajustar el ancho teniendo en cuenta todas la filas (no s�lo las visibles)
	//as� que se ecala la primera vez y despu�s se deja el control al usuario.
	//if(!settings->contains(COMICS_VIEW_HEADERS))
		comicView->resizeColumnsToContents();
	comicView->horizontalHeader()->setStretchLastSection(true);

	QStringList paths = dmCV->getPaths(currentPath());
	comicFlow->setImagePaths(paths);
	comicFlow->setMarks(dmCV->getReadList());
	comicFlow->setFocus(Qt::OtherFocusReason);

	checkEmptyFolder(&paths);

	if(paths.size()>0)
		comicView->setCurrentIndex(dmCV->index(0,0));
}

void LibraryWindow::checkEmptyFolder(QStringList * paths)
{
	if(paths == 0)
	{
		QStringList pathList = dmCV->getPaths(currentPath());
		paths = &pathList;
	}

	if(paths->size()>0 && !importedCovers)
	{
		disableComicsActions(false);
	}
	else
	{
		disableComicsActions(true);
		if(paths->size()>0)
			toggleFullScreenAction->setEnabled(true);
	}
}

void LibraryWindow::reloadCovers()
{
	loadCovers(foldersView->currentIndex());

	QModelIndex mi = dmCV->getIndexFromId(_comicIdEdited);
	comicView->scrollTo(mi,QAbstractItemView::PositionAtCenter);
	comicView->setCurrentIndex(mi);
	//centerComicFlow(mi);
	comicFlow->setCenterIndex(mi.row());
}

void LibraryWindow::centerComicFlow(const QModelIndex & mi)
{
	comicFlow->showSlide(mi.row());
	comicFlow->setFocus(Qt::OtherFocusReason);
}

void LibraryWindow::updateComicView(int i)
{
		QModelIndex mi = dmCV->index(i,2);
		comicView->setCurrentIndex(mi);
		comicView->scrollTo(mi,QAbstractItemView::EnsureVisible);
}

void LibraryWindow::openComic()
{
	if(!importedCovers)
	{
		ComicDB comic = dmCV->getComic(comicView->currentIndex());
        QString path = currentPath();
		QList<ComicDB> siblings = dmCV->getAllComics();

		quint64 comicId = comic.id;
		//TODO generate IDS for libraries...
		quint64 libraryId = selectedLibrary->currentIndex();
	
		//                 %1        %2      %3        NO-->%4          %5        %6        %7        %8         %9       %10
		//Invoke YACReader comicPath comicId libraryId NO-->currentPage bookmark1 bookmark2 bookmark3 brightness contrast gamma
        bool yacreaderFound = false;
#ifdef Q_OS_MAC
        QString comicIdS = QString("--comicId=") + QString("%1").arg(comicId);
        QString libraryIdS = QString("--libraryId=") + QString("%1").arg(libraryId);
        QString yacreaderPath = QDir::cleanPath(QCoreApplication::applicationDirPath()+"/../../../YACReader.app");
        if(yacreaderFound = QFileInfo(yacreaderPath).exists())
            QProcess::startDetached("open", QStringList() << "-n" << yacreaderPath << "--args" << path << comicIdS << libraryIdS ); /*<< page << bookmark1 << bookmark2 << bookmark3 << brightness << contrast << gamma*///,QStringList() << path);

#endif

#ifdef Q_OS_WIN																														  /* \"%4\" \"%5\" \"%6\" \"%7\" \"%8\" \"%9\" \"%10\" */
        yacreaderFound = QProcess::startDetached(QDir::cleanPath(QCoreApplication::applicationDirPath())+QString("/YACReader \"%1\" \"%2\" \"%3\"").arg(path).arg(QString("--comicId=") + QString::number(comicId)).arg(QString("--libraryId=") + QString::number(libraryId))/*.arg(page).arg(bookmark1).arg(bookmark2).arg(bookmark3).arg(brightness).arg(contrast).arg(gamma)*/,QStringList());
#endif

#if defined Q_OS_UNIX && !defined Q_OS_MAC
        QStringList parameters = QStringList() << path << (QString("--comicId=") + QString::number(comicId)) << (QString("--libraryId=") + QString::number(libraryId));
	yacreaderFound = QProcess::startDetached(QString("YACReader"),parameters);
#endif
        if(!yacreaderFound)
            QMessageBox::critical(this,tr("YACReader not found"),tr("YACReader not found, YACReader should be installed in the same folder as YACReaderLibrary."));

		setCurrentComicOpened();
	}
}

void LibraryWindow::setCurrentComicsStatusReaded(YACReaderComicReadStatus readStatus)
{

	comicFlow->setMarks(dmCV->setComicsRead(getSelectedComics(),readStatus));
	comicFlow->updateMarks();
}

void LibraryWindow::setCurrentComicReaded()
{
	this->setCurrentComicsStatusReaded(YACReader::Read);
}

void LibraryWindow::setCurrentComicOpened()
{

}

void LibraryWindow::setComicsReaded()
{
	comicFlow->setMarks(dmCV->setAllComicsRead(YACReader::Read));
	comicFlow->updateMarks();
}

void LibraryWindow::setCurrentComicUnreaded()
{
	this->setCurrentComicsStatusReaded(YACReader::Unread);
}

void LibraryWindow::setComicsUnreaded()
{
	comicFlow->setMarks(dmCV->setAllComicsRead(YACReader::Unread));
	comicFlow->updateMarks();
}

void LibraryWindow::createLibrary()
{
	createLibraryDialog->show(libraries);
}

void LibraryWindow::create(QString source, QString dest, QString name)
{
    QLOG_INFO() << QString("About to create a library from '%1' to '%2' with name '%3'").arg(source).arg(dest).arg(name);
	libraryCreator->createLibrary(source,dest);
	libraryCreator->start();
	_lastAdded = name;
	_sourceLastAdded = source;

	importWidget->setImportLook();
	showImportingWidget();

}

void LibraryWindow::reloadCurrentLibrary()
{
	loadLibrary(selectedLibrary->currentText());
}

void LibraryWindow::openLastCreated()
{
	
	selectedLibrary->disconnect();

	selectedLibrary->setCurrentIndex(selectedLibrary->findText(_lastAdded));
	libraries.addLibrary(_lastAdded,_sourceLastAdded);
	selectedLibrary->addItem(_lastAdded,_sourceLastAdded);
	selectedLibrary->setCurrentIndex(selectedLibrary->findText(_lastAdded));
	libraries.save();

	connect(selectedLibrary,SIGNAL(currentIndexChanged(QString)),this,SLOT(loadLibrary(QString)));
	
	loadLibrary(_lastAdded);
}

void LibraryWindow::showAddLibrary()
{
	addLibraryDialog->show();
}

void LibraryWindow::openLibrary(QString path, QString name)
{	
	if(!libraries.contains(name))
	{
		//TODO: fix bug, /a/b/c/.yacreaderlibrary/d/e
		path.remove("/.yacreaderlibrary");
		QDir d; //TODO change this by static methods (utils class?? with delTree for example) 
		if(d.exists(path + "/.yacreaderlibrary"))
		{
			_lastAdded = name;
			_sourceLastAdded = path;
			openLastCreated();
			addLibraryDialog->close();
		}
		else
			QMessageBox::warning(this,tr("Library not found"),tr("The selected folder doesn't contain any library."));
	}
	else
	{
		libraryAlreadyExists(name);
	}
}

void LibraryWindow::loadLibraries()
{
	libraries.load();
	foreach(QString name,libraries.getNames())
			selectedLibrary->addItem(name,libraries.getPath(name));
}


void LibraryWindow::saveLibraries()
{

	libraries.save();
	/*QFile f(QCoreApplication::applicationDirPath()+"/libraries.yacr");
	if(!f.open(QIODevice::WriteOnly))
	{
		QMessageBox::critical(NULL,tr("Saving libraries file...."),tr("There was a problem saving YACReaderLibrary libraries file. Please, check if you have enough permissions in the YACReader root folder."));
	}
	else
	{
		QTextStream txtS(&f);
		for(QMap<QString,QString>::iterator i = libraries.begin();i!=libraries.end();i++)
		{
			txtS << i.key() << "\n";
			txtS << i.value() << "\n";
		}
	}*/
}

void LibraryWindow::updateLibrary()
{
	importWidget->setUpdateLook();
	showImportingWidget();

	QString currentLibrary = selectedLibrary->currentText();
	QString path = libraries.getPath(currentLibrary);
	_lastAdded = currentLibrary;
	libraryCreator->updateLibrary(path,path+"/.yacreaderlibrary");
	libraryCreator->start();
}

void LibraryWindow::deleteCurrentLibrary()
{
	QString path = libraries.getPath(selectedLibrary->currentText());
	libraries.remove(selectedLibrary->currentText());
	selectedLibrary->removeItem(selectedLibrary->currentIndex());
	//selectedLibrary->setCurrentIndex(0);
	path = path+"/.yacreaderlibrary";

	QDir d(path);
	delTree(d);
	d.rmdir(path);
	if(libraries.isEmpty())//no more libraries available.
	{
		comicView->setModel(NULL);
		foldersView->setModel(NULL);
		comicFlow->clear();
        disableAllActions();
        showNoLibrariesWidget();
	}
	libraries.save();
}

void LibraryWindow::removeLibrary()
{
	QString currentLibrary = selectedLibrary->currentText();
	QMessageBox * messageBox = new QMessageBox(tr("Are you sure?"),tr("Do you want remove ")+currentLibrary+tr(" library?"),QMessageBox::Question,QMessageBox::Yes,QMessageBox::YesToAll,QMessageBox::No);
	messageBox->button(QMessageBox::YesToAll)->setText(tr("Remove and delete metadata"));
	int ret = messageBox->exec();
	if(ret == QMessageBox::Yes)
	{
		libraries.remove(currentLibrary);
		selectedLibrary->removeItem(selectedLibrary->currentIndex());
		//selectedLibrary->setCurrentIndex(0);
		if(libraries.isEmpty())//no more libraries available.
		{
			comicView->setModel(NULL);
			foldersView->setModel(NULL);
			comicFlow->clear();
            disableAllActions();
            showNoLibrariesWidget();
		}
		libraries.save();
	}
	else if(ret == QMessageBox::YesToAll)
	{
		deleteCurrentLibrary();
	}

}

void LibraryWindow::renameLibrary()
{
	renameLibraryDialog->show();
}

void LibraryWindow::rename(QString newName) //TODO replace
{
	QString currentLibrary = selectedLibrary->currentText();
	if(newName != currentLibrary)
	{
		if(!libraries.contains(newName))
		{
			libraries.rename(currentLibrary,newName);
			//selectedLibrary->removeItem(selectedLibrary->currentIndex());
			//libraries.addLibrary(newName,path);
			selectedLibrary->renameCurrentLibrary(newName);
			libraries.save();
			renameLibraryDialog->close();
#ifndef Q_OS_MAC
			if(!foldersView->currentIndex().isValid())
				libraryToolBar->setCurrentFolderName(selectedLibrary->currentText());
#endif
		}
		else
		{
			libraryAlreadyExists(newName);
		}
	}
	else
		renameLibraryDialog->close();
	//selectedLibrary->setCurrentIndex(selectedLibrary->findText(newName));
}

void LibraryWindow::cancelCreating()
{
	stopLibraryCreator();
}

void LibraryWindow::stopLibraryCreator()
{
	libraryCreator->stop();
	libraryCreator->wait();
}

void LibraryWindow::setRootIndex()
{
	if(!libraries.isEmpty())
	{	
		QString path=libraries.getPath(selectedLibrary->currentText())+"/.yacreaderlibrary";
		QDir d; //TODO change this by static methods (utils class?? with delTree for example) 
		if(d.exists(path))
		{
			loadCovers(QModelIndex());
			if(history.count()>1)
				updateHistory(QModelIndex());
		}
		else
		{
			comicView->setModel(NULL);
			comicFlow->clear();
		}

		foldersView->clearSelection();
	}

    setFolderAsNotCompletedAction->setVisible(false);
    setFolderAsCompletedAction->setVisible(false);
    setFolderAsFinishedAction->setVisible(false);
    setFolderAsNotFinishedAction->setVisible(false);
}


void LibraryWindow::toggleFullScreen()
{
	fullscreen?toNormal():toFullScreen();
	fullscreen = !fullscreen;
}

void LibraryWindow::toFullScreen()
{
	fromMaximized = this->isMaximized();

	comicFlow->hide();
	//comicFlow->setSlideSize(slideSizeF);
	comicFlow->setCenterIndex(comicFlow->centerIndex());
	comics->hide();
	sideBar->hide();
	libraryToolBar->hide();

	showFullScreen();

	comicFlow->show();
	comicFlow->setFocus(Qt::OtherFocusReason);

	fullScreenToolTip->move((width()-fullScreenToolTip->width())/2,0);
	fullScreenToolTip->adjustSize();
	fullScreenToolTip->show();
}

void LibraryWindow::toNormal()
{
	fullScreenToolTip->hide();
	comicFlow->hide();
	//comicFlow->setSlideSize(slideSizeW);
	comicFlow->setCenterIndex(comicFlow->centerIndex());
	comicFlow->render();
	comics->show();
	sideBar->show();
	
	comicFlow->show();

	if(fromMaximized)
		showMaximized();
	else
		showNormal();

#ifdef Q_OS_MAC
	QTimer * timer = new QTimer();
	timer->setSingleShot(true);
	timer->start();
	connect(timer,SIGNAL(timeout()),libraryToolBar,SLOT(show()));
	connect(timer,SIGNAL(timeout()),timer,SLOT(deleteLater()));
#else
	libraryToolBar->show();
#endif

}

void LibraryWindow::setFoldersFilter(QString filter)
{
	if(filter.isEmpty() && dm->isFilterEnabled())
	{
		dm->resetFilter();
		//foldersView->collapseAll();
		if(index != 0)
		{
			QModelIndex mi = dm->indexFromItem(index,column);
			foldersView->scrollTo(mi,QAbstractItemView::PositionAtTop);
			updateHistory(mi);
			foldersView->setCurrentIndex(mi);
		}
	}
	else
	{
		if(!filter.isEmpty())
		{
			dm->setFilter(filter, true);//includeComicsCheckBox->isChecked());
			foldersView->expandAll();
		}
	}
}

void LibraryWindow::showProperties()
{
	QModelIndexList indexList = getSelectedComics();

	QList<ComicDB> comics = dmCV->getComics(indexList);
	ComicDB c = comics[0];
	_comicIdEdited = c.id;//static_cast<TableItem*>(indexList[0].internalPointer())->data(4).toULongLong();

	propertiesDialog->databasePath = dm->getDatabase();
	propertiesDialog->basePath = currentPath();
	propertiesDialog->setComics(comics);
	
	propertiesDialog->show();
}

void LibraryWindow::showComicVineScraper()
{
	QModelIndexList indexList = getSelectedComics();

	QList<ComicDB> comics = dmCV->getComics(indexList);
	ComicDB c = comics[0];
	_comicIdEdited = c.id;//static_cast<TableItem*>(indexList[0].internalPointer())->data(4).toULongLong();

	comicVineDialog->databasePath = dm->getDatabase();
	comicVineDialog->basePath = currentPath();
	comicVineDialog->setComics(comics);

	comicVineDialog->show();
}

void LibraryWindow::setRemoveError()
{
	removeError = true;
}

void LibraryWindow::checkRemoveError()
{
	if(removeError)
	{
		QMessageBox::critical(this,tr("Unable to delete"),tr("There was an issue trying to delete the selected comics. Please, check for write permissions in the selected files or containing folder."));
	}
    removeError = false;
}

void LibraryWindow::resetComicRating()
{
    QModelIndexList indexList = getSelectedComics();

    dmCV->startTransaction();
    for(auto & index:indexList)
    {
        dmCV->resetComicRating(index);
    }
    dmCV->finishTransaction();
}

void LibraryWindow::asignNumbers()
{
	QModelIndexList indexList = getSelectedComics();

	int startingNumber = indexList[0].row()+1;
	if(indexList.count()>1)
	{
		bool ok;
		int n = QInputDialog::getInt(this, tr("Asign comics numbers"),
			tr("Asign numbers starting in:"), startingNumber,0,2147483647,1,&ok);
		if (ok)
			startingNumber = n;
		else
			return;
	}
	_comicIdEdited = dmCV->asignNumbers(indexList,startingNumber);
	
	reloadCovers();
}

void LibraryWindow::openContainingFolderComic()
{
QModelIndex modelIndex = comicView->currentIndex();
QFileInfo file = QDir::cleanPath(currentPath() + dmCV->getComicPath(modelIndex)); 
#if defined Q_OS_UNIX && !defined Q_OS_MAC
	QString path = file.absolutePath();
	QDesktopServices::openUrl(QUrl("file:///"+path, QUrl::TolerantMode));
#endif

#ifdef Q_OS_MAC
	QString filePath = file.absoluteFilePath();
	QStringList args;
	args << "-e";
	args << "tell application \"Finder\"";
	args << "-e";
	args << "activate";
	args << "-e";
	args << "select POSIX file \""+filePath+"\"";
	args << "-e";
	args << "end tell";
	QProcess::startDetached("osascript", args);
#endif
	
#ifdef Q_OS_WIN
    QString filePath = file.absoluteFilePath();
    QString cmdArgs = QString("/select,\"") + QDir::toNativeSeparators(filePath) + QStringLiteral("\"");
    ShellExecuteW(0, L"open", L"explorer.exe", reinterpret_cast<LPCWSTR>(cmdArgs.utf16()), 0, SW_NORMAL);
#endif
}

void LibraryWindow::openContainingFolder()
{
	QModelIndex modelIndex = foldersView->currentIndex();
    QString path;
    if(modelIndex.isValid())
        path = QDir::cleanPath(currentPath() + dm->getFolderPath(modelIndex));
    else
        path = QDir::cleanPath(currentPath());
    QDesktopServices::openUrl(QUrl("file:///"+path, QUrl::TolerantMode));
}

void LibraryWindow::setFolderAsNotCompleted()
{
    dm->updateFolderCompletedStatus(foldersView->selectionModel()->selectedRows(),false);
}

void LibraryWindow::setFolderAsCompleted()
{
    dm->updateFolderCompletedStatus(foldersView->selectionModel()->selectedRows(),true);
}

void LibraryWindow::setFolderAsFinished()
{
    dm->updateFolderFinishedStatus(foldersView->selectionModel()->selectedRows(),true);
}

void LibraryWindow::setFolderAsNotFinished()
{
   dm->updateFolderFinishedStatus(foldersView->selectionModel()->selectedRows(),false);
}

void LibraryWindow::exportLibrary(QString destPath)
{
	QString currentLibrary = selectedLibrary->currentText();
	QString path = libraries.getPath(currentLibrary)+"/.yacreaderlibrary";
	packageManager->createPackage(path,destPath+"/"+currentLibrary);
}

void LibraryWindow::importLibrary(QString clc,QString destPath,QString name)
{
	packageManager->extractPackage(clc,destPath+"/"+name);
	_lastAdded = name;
	_sourceLastAdded = destPath+"/"+name;
}

void LibraryWindow::reloadOptions()
{
	//comicFlow->setFlowType(flowType);
	comicFlow->updateConfig(settings);
}

QString LibraryWindow::currentPath()
{
	return libraries.getPath(selectedLibrary->currentText());
}

void LibraryWindow::hideComicFlow(bool hide)
{
	if(hide)
	{
		QList<int> sizes;
		sizes.append(0);
		int total = sVertical->sizes().at(0) + sVertical->sizes().at(1);
		sizes.append(total);
		sVertical->setSizes(sizes);	
	}
	else
	{
		QList<int> sizes;
		int total = sVertical->sizes().at(0) + sVertical->sizes().at(1);
		sizes.append(2*total/3);
		sizes.append(total/3);
		sVertical->setSizes(sizes);	
	}

}

void LibraryWindow::showExportComicsInfo()
{
	exportComicsInfoDialog->source = currentPath() + "/.yacreaderlibrary/library.ydb";
	exportComicsInfoDialog->show();
}

void LibraryWindow::showImportComicsInfo()
{
	importComicsInfoDialog->dest = currentPath() + "/.yacreaderlibrary/library.ydb";
	importComicsInfoDialog->show();
}

void LibraryWindow::closeEvent ( QCloseEvent * event )
{
	settings->setValue(MAIN_WINDOW_GEOMETRY, saveGeometry());
	settings->setValue(COMICS_VIEW_HEADERS,comicView->horizontalHeader()->saveState());
	event->accept();
	//settings->setValue(COMICS_VIEW_HEADERS_GEOMETRY,comicView->horizontalHeader()->saveGeometry());
}

void LibraryWindow::showNoLibrariesWidget()
{
	disableAllActions();
	foldersFilter->setDisabled(true);
	mainWidget->setCurrentIndex(1);
}

void LibraryWindow::showRootWidget()
{
	libraryToolBar->setDisabled(false);
	foldersFilter->setEnabled(true);
	mainWidget->setCurrentIndex(0);
}

void LibraryWindow::showImportingWidget()
{
	disableAllActions();
	importWidget->clear();
	libraryToolBar->setDisabled(true);
	foldersFilter->setDisabled(true);
	mainWidget->setCurrentIndex(2);
}

void LibraryWindow::manageCreatingError(const QString & error)
{
	QMessageBox::critical(this,tr("Error creating the library"),error);
}

void LibraryWindow::manageUpdatingError(const QString & error)
{
	QMessageBox::critical(this,tr("Error updating the library"),error);
}

void LibraryWindow::manageOpeningLibraryError(const QString & error)
{
	QMessageBox::critical(this,tr("Error opening the library"),error);
}

bool lessThanModelIndexRow(const QModelIndex & m1, const QModelIndex & m2)
{
	return m1.row()<m2.row();
}

QModelIndexList LibraryWindow::getSelectedComics()
{
	//se fuerza a que haya almenos una fila seleccionada TODO comprobar se se puede forzar a la tabla a que lo haga autom�ticamente
	QModelIndexList selection = comicView->selectionModel()->selectedRows();
	
	qSort(selection.begin(),selection.end(),lessThanModelIndexRow);

	if(selection.count()==0)
	{
		comicView->selectRow(comicFlow->centerIndex());
		selection = comicView->selectionModel()->selectedRows();
	}
	return selection;
}

void LibraryWindow::deleteComics()
{
	int ret = QMessageBox::question(this,tr("Delete comics"),tr("All the selected comics will be deleted from your disk. Are you sure?"),QMessageBox::Yes,QMessageBox::No);

	if(ret == QMessageBox::Yes)
	{

		QModelIndexList indexList = getSelectedComics();

		QList<ComicDB> comics = dmCV->getComics(indexList);

		QList<QString> paths;
		QString libraryPath = currentPath();
		foreach(ComicDB comic, comics)
		{
			paths.append(libraryPath + comic.path);
		}

		ComicsRemover * remover = new ComicsRemover(indexList,paths);

		//comicView->showDeleteProgress();
		dmCV->startTransaction();

		connect(remover, SIGNAL(remove(int)), dmCV, SLOT(remove(int)));
		connect(remover, SIGNAL(remove(int)), comicFlow, SLOT(remove(int)));
		connect(remover,SIGNAL(removeError()),this,SLOT(setRemoveError()));
		connect(remover, SIGNAL(finished()), dmCV, SLOT(finishTransaction()));
		//connect(remover, SIGNAL(finished()), comicView, SLOT(hideDeleteProgress()));
		connect(remover, SIGNAL(finished()),this,SLOT(checkEmptyFolder()));
		connect(remover, SIGNAL(finished()),this,SLOT(checkRemoveError()));
		connect(remover, SIGNAL(finished()), remover, SLOT(deleteLater()));
		//connect(remover, SIGNAL(finished()), thread, SLOT(deleteLater()));

		remover->start();
	}
}

/*
void LibraryWindow::showSocial()
{	
	socialDialog->move(this->mapToGlobal(QPoint(width()-socialDialog->width()-10, centralWidget()->pos().y()+10)));

	QModelIndexList indexList = getSelectedComics();

	ComicDB comic = dmCV->getComic(indexList.at(0));

	socialDialog->setComic(comic,currentPath());
	socialDialog->setHidden(false);
}*/

void LibraryWindow::backward()
{
	if(currentFolderNavigation>0)
	{
		currentFolderNavigation--;
		loadCovers(history.at(currentFolderNavigation));
		foldersView->setCurrentIndex(history.at(currentFolderNavigation));
		forwardAction->setEnabled(true);
	}
	if(currentFolderNavigation==0)
	{
		backAction->setEnabled(false);
	}
}

void LibraryWindow::forward()
{
	if(currentFolderNavigation<history.count()-1)
	{
		currentFolderNavigation++;
		loadCovers(history.at(currentFolderNavigation));
		foldersView->setCurrentIndex(history.at(currentFolderNavigation));
		backAction->setEnabled(true);
	}
	if(currentFolderNavigation==history.count()-1)
	{
		forwardAction->setEnabled(false);
	}
}

void LibraryWindow::updateHistory(const QModelIndex &mi)
{
	//remove history from current index
	if(!mi.isValid())
		return;
	int numElementsToRemove = history.count() - (currentFolderNavigation+1);
	while(numElementsToRemove>0)
	{
		numElementsToRemove--;
		history.removeLast();
	}

	if(mi!=history.at(currentFolderNavigation))
	{
		history.append(mi);

		backAction->setEnabled(true);
		currentFolderNavigation++;
	}

    forwardAction->setEnabled(false);
}

void LibraryWindow::updateFoldersViewConextMenu(const QModelIndex &mi)
{
    if(!mi.isValid())
        return;

    TreeItem * item = static_cast<TreeItem *>(mi.internalPointer());
    bool isFinished = item->data(TreeModel::Finished).toBool();
    bool isCompleted = item->data(TreeModel::Completed).toBool();

    setFolderAsFinishedAction->setVisible(!isFinished);
    setFolderAsNotFinishedAction->setVisible(isFinished);

    setFolderAsCompletedAction->setVisible(!isCompleted);
    setFolderAsNotCompletedAction->setVisible(isCompleted);
}

void LibraryWindow::libraryAlreadyExists(const QString & name)
{
	QMessageBox::information(this,tr("Library name already exists"),tr("There is another library with the name '%1'.").arg(name));
}

void LibraryWindow::importLibraryPackage()
{
	importLibraryDialog->show(libraries);
}

void LibraryWindow::updateComicsView(quint64 libraryId, const ComicDB & comic)
{
	//TODO comprobar la biblioteca....
	if(libraryId == selectedLibrary->currentIndex())
	{
		dmCV->reload(comic);
		comicFlow->setMarks(dmCV->getReadList());
	}
}
